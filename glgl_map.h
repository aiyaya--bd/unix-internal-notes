#ifndef GLGL_MAP_H
#define GLGL_MAP_H

#include <stdexcept>
#include "sub/glgl_tree_base.h"
#include "glgl_tuple.h"

namespace glglT {

template <typename K, typename T,
          typename Comp = less<K>, typename Alloc = allocator<K>>
class map: public tree_base<map<K, T, Comp, Alloc>, pair<const K, T>, Comp, Alloc> {
    friend class tree_base<map, pair<const K, T>, Comp, Alloc>;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        rebind_ptr rebind_ptr;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        rebind_traits rebind_traits;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        insert_return_type insert_return_type;
public:
    typedef T mapped_type;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        key_compare key_compare;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        key_type key_type;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        value_type value_type;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        size_type size_type;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        iterator iterator;
    typedef typename tree_base<map, pair<const K, T>, Comp, Alloc>::
        const_iterator const_iterator;

    struct value_compare: function_type<bool (value_type, value_type)> {
        friend class map;
    protected:
        Comp c;
        explicit value_compare(Comp p) noexcept(noexcept(Comp(p))): c(p) {}
    public:
        bool operator()(const value_type &v1, const value_type &v2) const
        {
            return c(v1.first, v2.first);
        }
    };

    explicit map(const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<map, pair<const K, T>, Comp, Alloc>(cp, ac) {}

    explicit map(const Alloc &ac): tree_base<map, pair<const K, T>, Comp, Alloc>(ac) {}

    template <typename It>
    map(It b, It e, const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<map, pair<const K, T>, Comp, Alloc>(b, e, cp, ac) {}

    map(const map &o): tree_base<map, pair<const K, T>, Comp, Alloc>(o) {}

    map(const map &o, const Alloc &ac):
        tree_base<map, pair<const K, T>, Comp, Alloc>(o, ac) {}

    map(map &&o): tree_base<map, pair<const K, T>, Comp, Alloc>(glglT::move(o)) {}

    map(map &&o, const Alloc &ac):
        tree_base<map, pair<const K, T>, Comp, Alloc>(glglT::move(o), ac) {}

    map(std::initializer_list<pair<const K, T>> il,
        const Comp &cp = Comp(), const Alloc &ac = Alloc()):
            tree_base<map, pair<const K, T>, Comp, Alloc>(il, cp, ac) {}

    map &operator=(const map &o)
    {
        tree_base<map, pair<const K, T>, Comp, Alloc>::operator=(o);
        return *this;
    }

    map &operator=(map &&o)
    {
        tree_base<map, pair<const K, T>, Comp, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    map &operator=(std::initializer_list<pair<const K, T>> il)
    {
        tree_base<map, pair<const K, T>, Comp, Alloc>::operator=(il);
        return *this;
    }

    T &operator[](const key_type &k)
    {
        const auto itr = this->find(k);
        if (itr.base() == this->eot)
            return this->emplace(piecewise_construct,
                                 glglT::forward_as_tuple(k),
                                 glglT::tuple<>()).first->second;
        else
            return itr.base()->v.second;
    }

    T &operator[](key_type &&k)
    {
        const auto itr = this->find(k);
        if (itr.base() == this->eot)
            return this->emplace(piecewise_construct,
                                 glglT::forward_as_tuple(glglT::move(k)),
                                 glglT::tuple<>()).first->second;
        else
            return itr.base()->v.second;
    }

    T &at(const key_type &k)
    {
        return const_cast<T &>(const_cast<const map *>(this)->at(k));
    }

    const T &at(const key_type &k) const
    {
        const auto itr = this->find(k);
        if (itr.base() == this->eot)
            throw std::out_of_range("not found in map");
        return itr->second;
    }

    size_type count(const key_type &k) const
    {
        return this->find(k).base() != this->eot;
    }

    pair<const_iterator, const_iterator> equal_range(const key_type &k) const
    {
        const_iterator itr1 = this->not_smaller(k), itr2 = itr1;
        if (!this->c(k, itr1->first))
            ++itr2;
        return glglT::make_pair(glglT::move(itr1), glglT::move(itr2));
    }

    key_compare key_comp() const  { this->c; }
    value_compare value_comp() const { value_compare(this->c); }
private:
    static const key_type &get_key(const value_type &v) noexcept { return v.first; }
    static iterator get(insert_return_type &&r) noexcept
    {
        return glglT::move(r.first);
    }

    insert_return_type insert_equal(rebind_ptr root, rebind_ptr node)
    {
        if (this->len == 0) {
            ++this->len;
            this->eot->p = this->eot->l = this->eot->r = node;
            node->p = node->l = node->r = this->eot;
            node->is_red = false;
            return glglT::make_pair(iterator(node, this->eot), true);
        } else {
            const auto old_len = this->len;
            this->insert_equal(root, node, !this->c(node->v.first, root->v.first));
            return glglT::make_pair(iterator(node, this->eot), old_len != this->len);
        }
    }

    void insert_equal(rebind_ptr root, rebind_ptr &node, bool node_not_smaller)
    {
        for (; ; node_not_smaller = !this->c(node->v.first, root->v.first)) {
            const bool node_not_bigger = !this->c(root->v.first, node->v.first);
            if (node_not_bigger && node_not_smaller) {
                rebind_traits::destroy(this->a, node);
                rebind_traits::deallocate(this->a, node, 1);
                node = root;
                return;
            } else if (node_not_bigger) {
                if (root->l == this->eot) {
                    ++this->len;
                    root->l = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->r == root)
                        this->eot->r = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->l;
            } else {
                if (root->r == this->eot) {
                    ++this->len;
                    root->r = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->l == root)
                        this->eot->l = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->r;
            }
        }
    }
};

template <typename K, typename T, typename C, typename A> inline
void swap(map<K, T, C, A> &b1, map<K, T, C, A> &b2)
{
    b1.swap(b2);
}

template <typename K, typename T,
          typename Comp = less<K>, typename Alloc = allocator<K>>
class multimap: public tree_base<multimap<K, T, Comp, Alloc>, pair<const K, T>, Comp, Alloc> {
    friend class tree_base<multimap, pair<const K, T>, Comp, Alloc>;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        rebind_ptr rebind_ptr;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        rebind_traits rebind_traits;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        insert_return_type insert_return_type;
public:
    typedef T mapped_type;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        key_compare key_compare;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        key_type key_type;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        value_type value_type;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        size_type size_type;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        iterator iterator;
    typedef typename tree_base<multimap, pair<const K, T>, Comp, Alloc>::
        const_iterator const_iterator;

    struct value_compare: function_type<bool (value_type, value_type)> {
        friend class multimap;
    protected:
        Comp c;
        explicit value_compare(Comp p) noexcept(noexcept(Comp(p))): c(p) {}
    public:
        bool operator()(const value_type &v1, const value_type &v2) const
        {
            return c(v1.first, v2.first);
        }
    };

    explicit multimap(const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(cp, ac) {}

    explicit multimap(const Alloc &ac):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(ac) {}

    template <typename It>
    multimap(It b, It e, const Comp &cp = Comp(), const Alloc &ac = Alloc()):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(b, e, cp, ac) {}

    multimap(const multimap &o):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(o) {}

    multimap(const multimap &o, const Alloc &ac):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(o, ac) {}

    multimap(multimap &&o):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(glglT::move(o)) {}

    multimap(multimap &&o, const Alloc &ac):
        tree_base<multimap, pair<const K, T>, Comp, Alloc>(glglT::move(o), ac) {}

    multimap(std::initializer_list<pair<const K, T>> il,
        const Comp &cp = Comp(), const Alloc &ac = Alloc()):
            tree_base<multimap, pair<const K, T>, Comp, Alloc>(il, cp, ac) {}

    multimap &operator=(const multimap &o)
    {
        tree_base<multimap, pair<const K, T>, Comp, Alloc>::operator=(o);
        return *this;
    }

    multimap &operator=(multimap &&o)
    {
        tree_base<multimap, pair<const K, T>, Comp, Alloc>::operator=(glglT::move(o));
        return *this;
    }

    multimap &operator=(std::initializer_list<pair<const K, T>> il)
    {
        tree_base<multimap, pair<const K, T>, Comp, Alloc>::operator=(il);
        return *this;
    }

    size_type count(const key_type &k) const
    {
        const const_iterator itr = this->find(k);
        if (itr.base() != this->eot) {
            size_type n = 1;
            for (const_iterator prev = itr; (--prev).base() != this->eot; ++n)
                if (this->c(prev.base()->v.first, itr.base()->v.first))
                    break;
            for (const_iterator next = itr; (++next).base() != this->eot; ++n)
                if (this->c(itr.base()->v.first, next.base()->v.first))
                    break;
            return n;
        } else
            return 0;
    }

    pair<const_iterator, const_iterator> equal_range(const key_type &k) const
    {
        return glglT::make_pair(this->not_smaller(k), this->bigger(k));
    }

    key_compare key_comp() const { this->c; }
    value_compare value_comp() const { value_compare(this->c); }
private:
    static iterator get(insert_return_type &&r) noexcept { return glglT::move(r); }
    static const key_type &get_key(const value_type &v) noexcept { return v.first; }

    insert_return_type insert_equal(rebind_ptr root, rebind_ptr node)
    {
        if (this->len == 0) {
            ++this->len;
            this->eot->p = this->eot->l = this->eot->r = node;
            node->p = node->l = node->r = this->eot;
            node->is_red = false;
        } else
            this->insert_equal(root, node, !this->c(node->v.first, root->v.first));
        return iterator(node, this->eot);
    }

    void insert_equal(rebind_ptr root, rebind_ptr &node, bool node_not_smaller)
    {
        for (; ; node_not_smaller = !this->c(node->v.first, root->v.first)) {
            if (node_not_smaller) {
                if (root->r == this->eot) {
                    ++this->len;
                    root->r = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->l == root)
                        this->eot->l = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->r;
            } else {
                if (root->l == this->eot) {
                    ++this->len;
                    root->l = node;
                    node->p = root;
                    node->l = node->r = this->eot;
                    if (this->eot->r == root)
                        this->eot->r = node;
                    this->insert_fix(node, root);
                    return;
                }   root = root->l;
            }
        }
    }
};

template <typename K, typename T, typename C, typename A> inline
void swap(multimap<K, T, C, A> &b1, multimap<K, T, C, A> &b2)
{
    b1.swap(b2);
}

} // namespace glglT

#endif // GLGL_MAP_H

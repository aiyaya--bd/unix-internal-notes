#ifndef GLGL_FUNCTIONAL_BASE_H
#define GLGL_FUNCTIONAL_BASE_H

#include "glgl_type_tool.h"

namespace glglT {

template <typename, typename = type_helper<>>
struct may_result_type {};

template <typename T>
struct may_result_type<T, type_helper<typename T::result_type>> {
    typedef typename T::result_type result_type;
};

template <typename ResT, typename... Args>
struct may_result_type<ResT (Args...), type_helper<ResT>> {
    typedef ResT result_type;
};

template <typename ResT, typename... Args>
struct may_result_type<ResT (Args..., ...), type_helper<ResT>> {
    typedef ResT result_type;
};

template <typename, typename = type_helper<>>
struct may_argument_type {};

template <typename T>
struct may_argument_type<T, type_helper<typename T::argument_type>> {
    typedef typename T::argument_type argument_type;
};

template <typename ResT, typename Arg>
struct may_argument_type<ResT (Arg), type_helper<Arg>> {
    typedef Arg argument_type;
};

template <typename, typename = type_helper<>>
struct may_first_argument_type {};

template <typename T>
struct may_first_argument_type<T,
                               type_helper<typename T::first_argument_type>> {
    typedef typename T::first_argument_type first_argument_type;
};

template <typename ResT, typename Arg1, typename Arg2>
struct may_first_argument_type<ResT (Arg1, Arg2), type_helper<Arg1>> {
    typedef Arg1 first_argument_type;
};

template <typename, typename = type_helper<>>
struct may_second_argument_type {};

template <typename T>
struct may_second_argument_type<T,
                                type_helper<typename T::second_argument_type>> {
    typedef typename T::second_argument_type second_argument_type;
};

template <typename ResT, typename Arg1, typename Arg2>
struct may_second_argument_type<ResT (Arg1, Arg2), type_helper<Arg2>> {
    typedef Arg2 second_argument_type;
};

template <typename T>
struct function_type_base:
    may_result_type<T>,
    may_argument_type<T>,
    may_first_argument_type<T>,
    may_second_argument_type<T>
{};

template <typename ResT, typename ClassT>
struct function_type_base<ResT ClassT::*>:
    function_type_base<ResT (ClassT *)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*)(Args...)>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*)(Args..., ...)>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename... Args>
struct function_type_base<ResT (*) (Args...)>:
    function_type_base<ResT (Args...)>
{};

template <typename ResT, typename... Args>
struct function_type_base<ResT (*) (Args..., ...)>:
    function_type_base<ResT (Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) const>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) volatile>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) volatile>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) const volatile>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const volatile>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) &>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) &>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) const &>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const &>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) volatile &>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) volatile &>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) const volatile &>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const volatile &>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) &&>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) &&>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) const &&>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const &&>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args...) volatile &&>:
    function_type_base<ResT (ClassT *, Args...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) volatile &&>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename ResT, typename ClassT, typename... Args>
struct function_type_base<ResT (ClassT::*) (Args..., ...) const volatile &&>:
    function_type_base<ResT (ClassT *, Args..., ...)>
{};

template <typename T>
struct function_type: function_type_base<
    typename remove_cv<typename remove_reference<T>::type>::type>
{};


} // namespace glglT

#endif // GLGL_FUNCTIONAL_BASE_H

#ifndef GLGL_BASIC_ITERATOR_H
#define GLGL_BASIC_ITERATOR_H

#include <iterator>

namespace glglT {

using std::iterator;

using std::input_iterator_tag;

using std::output_iterator_tag;

using std::forward_iterator_tag;

using std::bidirectional_iterator_tag;

using std::random_access_iterator_tag;

} // namespace glglT


#endif // GLGL_BASIC_ITERATOR_H

#ifndef GLGL_LITERAL_TEMPLATE_H
#define GLGL_LITERAL_TEMPLATE_H

#include "glgl_base_algo.h"
#include "glgl_tuple_im.h"

namespace glglT {

template <typename Ch, Ch, typename> struct glgl_string_literal_kmp_index;

// tools to get kmp_index

template <bool, size_t, typename Ch, Ch, Ch...>
struct glgl_string_literal_last_nth_aux;

template <size_t N, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_last_nth:
    glgl_string_literal_last_nth_aux<N == sizeof...(cs), N, Ch, c, cs...> {
};

template <size_t N, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_last_nth_aux<true, N, Ch, c, cs...>:
    integral_constant<char, c> {
};

template <size_t N, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_last_nth_aux<false, N, Ch, c, cs...>:
    glgl_string_literal_last_nth<N, Ch, cs...> {
};

template <bool, size_t, typename Ch, Ch, Ch...>
struct glgl_string_literal_reverse_aux;

template <size_t Ig, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_reverse:
    glgl_string_literal_reverse_aux<Ig == sizeof...(cs), Ig, Ch, c, cs...> {
};

template <size_t Ig, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_reverse_aux<true, Ig, Ch, c, cs...> {
    typedef glgl_string_literal_kmp_index<Ch, c, void> index;
};

template <size_t Ig, typename Ch, Ch c, Ch... cs>
struct glgl_string_literal_reverse_aux<false, Ig, Ch, c, cs...> {
    typedef glgl_string_literal_kmp_index<Ch,
        glgl_string_literal_last_nth<Ig, Ch, c, cs...>::value,
        typename glgl_string_literal_reverse<Ig + 1, Ch, c, cs...>::index
    > index;
};

// tools to get kmp_index end

// tools to get information from kmp_index

template <typename> struct glgl_string_literal_kmp_index_sizeof;

template <>
struct glgl_string_literal_kmp_index_sizeof<void>:
    integral_constant<size_t, 0> {
};

template <typename Ch, Ch c, typename I>
struct glgl_string_literal_kmp_index_sizeof<glgl_string_literal_kmp_index<Ch, c, I>>:
    integral_constant<size_t, glgl_string_literal_kmp_index_sizeof<I>::value + 1> {
};

template <typename Ch, Ch c, typename I, typename IC, typename = integral_constant
          <bool, IC::value == glgl_string_literal_kmp_index_sizeof<I>::value>>
struct glgl_string_literal_kmp_index_get_char;

template <typename Ch, Ch c, typename I, typename IC>
struct glgl_string_literal_kmp_index_get_char<Ch, c, I, IC, true_type>:
    integral_constant<Ch, c> {
};

template <typename Ch, Ch C, Ch c, typename I, typename IC>
struct glgl_string_literal_kmp_index_get_char<
    Ch, C, glgl_string_literal_kmp_index<Ch, c, I>, IC, false_type>:
    glgl_string_literal_kmp_index_get_char<Ch, c, I, IC> {
};

template <typename Ch, Ch c, typename I, typename IC, typename = integral_constant
          <bool, IC::value == glgl_string_literal_kmp_index_sizeof<I>::value>>
struct glgl_string_literal_kmp_index_get;

template <typename Ch, Ch c, typename I, typename IC>
struct glgl_string_literal_kmp_index_get<Ch, c, I, IC, true_type> {
    typedef glgl_string_literal_kmp_index<Ch, c, I> type;
};

template <typename Ch, Ch C, Ch c, typename I, typename IC>
struct glgl_string_literal_kmp_index_get<
    Ch, C, glgl_string_literal_kmp_index<Ch, c, I>, IC, false_type>:
    glgl_string_literal_kmp_index_get<Ch, c, I, IC> {
};

// tools to get information from kmp_index end

template <size_t prefix, size_t curr, typename Ch, Ch c, typename I,
          typename = integral_constant<
              bool,
              glgl_string_literal_kmp_index_get_char<
                  Ch, c, I, integral_constant<size_t, prefix>>::value ==
              glgl_string_literal_kmp_index_get_char<
                  Ch, c, I, integral_constant<size_t, curr>>::value>>
struct glgl_string_literal_kmp_build: integral_constant<size_t, prefix + 1> {
    typedef typename glgl_string_literal_kmp_index_get<
        Ch, c, I, integral_constant<size_t, prefix>>::type element;
}; // inherit next prefix, hold table value

template <size_t prefix, size_t curr, typename Ch, Ch c, typename I,
          typename = integral_constant<
              bool,
              glgl_string_literal_kmp_index_get_char<
                  Ch, c, I, integral_constant<size_t, prefix>>::value ==
              glgl_string_literal_kmp_index_get_char<
                  Ch, c, I, integral_constant<size_t, curr>>::value>,
          typename = integral_constant<bool, prefix == 0>>
struct glgl_string_literal_kmp_build_until_true:
    integral_constant<size_t, prefix + 1> {
};

template <size_t prefix, size_t curr, typename Ch, Ch c, typename I>
struct glgl_string_literal_kmp_build_until_true<prefix, curr, Ch, c, I,
                                                false_type, false_type>:
    glgl_string_literal_kmp_build_until_true<
        glgl_string_literal_kmp_index_get<
            Ch, c, I, integral_constant<size_t, prefix>>::type::value, curr, Ch, c, I> {
};

template <size_t prefix, size_t curr, typename Ch, Ch c, typename I>
struct glgl_string_literal_kmp_build_until_true<prefix, curr, Ch, c, I,
                                                false_type, true_type>:
    integral_constant<size_t, 0> {
};

template <size_t prefix, size_t curr, typename Ch, Ch c, typename I>
struct glgl_string_literal_kmp_build<prefix, curr, Ch, c, I, false_type>:
    glgl_string_literal_kmp_build_until_true<prefix, curr, Ch, c, I> {
    typedef integral_constant<size_t, prefix> element;
}; // inherit next prefix, hold table value

template <typename Ch, Ch c>
struct glgl_string_literal_kmp_index<Ch, c, void>: integral_constant<size_t, 0> {
    template <Ch C> using build =
        glgl_string_literal_kmp_build<0, 1, Ch, C, glgl_string_literal_kmp_index>;
    typedef integral_constant<Ch, c> char_constant;
};

template <typename Ch, Ch c, Ch C, typename I>
struct glgl_string_literal_kmp_index<Ch, c, glgl_string_literal_kmp_index<Ch, C, I>>:
    glgl_string_literal_kmp_index<Ch, C, I>::template build<c>::element {
    template <char O> using build = glgl_string_literal_kmp_build<
        glgl_string_literal_kmp_index<Ch, C, I>::template build<c>::value,
        glgl_string_literal_kmp_index_sizeof<glgl_string_literal_kmp_index>::value,
        Ch, O, glgl_string_literal_kmp_index>;
    typedef integral_constant<Ch, c> char_constant;
};

template <typename Char, Char...> struct kmp_literal;

template <typename T>
struct kmp_literal<T> {
    template <typename It, typename =
              type_helper<decltype(glglT::declval<T>() == *(glglT::declval<It>()))>>
    pair<It, It> operator()(It b, It e) const { return glglT::make_pair(b, b); }
};

template <typename Ch, Ch V, Ch... cs>
struct kmp_literal<Ch, V, cs...> {
    template <typename It>
    pair<It, It> operator()(It first, It last) const
    {
        last = glglT::real_last(first, last);
        typedef typename glgl_string_literal_reverse<0, Ch, V, cs...>::index index;
        size_t i = 0;
        auto p = kmp_literal::forward(first, last, i, first, index());
        return p == last && i == 0
            ? glglT::make_pair(last, last) : glglT::make_pair(++first, p);
    }
private:
    template <typename It, Ch c, typename I> static
    It backward(It curr, It last, size_t &i, It &begin,
                glgl_string_literal_kmp_index<Ch, c, I>)
    {
        typedef typename glgl_string_literal_kmp_index_get
            <Ch, c, I, integral_constant
             <size_t, glgl_string_literal_kmp_index<Ch, c, I>::value>>::type index;
        typedef typename glgl_string_literal_kmp_index_get
            <Ch, c, I, integral_constant
             <size_t, glgl_string_literal_kmp_index<Ch, c, I>::value + 1>>::type next;
        if (*curr == index::char_constant::value) {
            (i == sizeof...(cs) + 1) ? (++begin, i) : ++i;
            return kmp_literal::forward(++curr, last, i, begin, next(),
                                        glgl_string_literal_kmp_index<Ch, c, I>());
        }
        curr = kmp_literal::backward(curr, last, i, begin, index());
        if (curr == last)
            return i = 0, last;
        return kmp_literal::forward(curr, last, i, begin, next(),
                                    glgl_string_literal_kmp_index<Ch, c, I>());
    }

    template <typename It, Ch c> static
    It backward(It curr, It last, size_t &i, It &begin,
                glgl_string_literal_kmp_index<Ch, c, void>)
    {
        while (true) {
            (i == sizeof...(cs) + 1) ? (++begin, i) : ++i;
            if (*curr == c)
                return ++curr;
            if (++curr == last)
                return i = 0, last;
        }
    }

    template <typename It, Ch f, Ch t, typename From, typename To,
              typename = enable_if<!is_same<From, To>::value>> static
    It forward(It curr, It last, size_t &i, It &begin,
               glgl_string_literal_kmp_index<Ch, f, From>,
               glgl_string_literal_kmp_index<Ch, t, To>)
    {
        while (true) {
            if (*curr == f) {
                (i == sizeof...(cs) + 1) ? (++begin, i) : ++i;
                return kmp_literal::forward
                    (++curr, last, i, begin,
                     typename glgl_string_literal_kmp_index_get<
                     Ch, t, To, integral_constant<
                     size_t, glgl_string_literal_kmp_index_sizeof<
                     glgl_string_literal_kmp_index<Ch, f, From>>::value>>::type(),
                     glgl_string_literal_kmp_index<Ch, t, To>());;
            }
            curr = kmp_literal::backward
                (curr, last, i, begin, glgl_string_literal_kmp_index<Ch, f, From>());
            if (curr == last)
                return i = 0, last;
        }
    }

    template <typename It, Ch c, typename I> static
    It forward(It curr, It last, size_t &i, It &begin,
               glgl_string_literal_kmp_index<Ch, c, I>,
               glgl_string_literal_kmp_index<Ch, c, I>)
    {
        return curr;
    }

    template <typename It, Ch c, typename I> static
    It forward(It first, It last, size_t &i, It &begin,
               glgl_string_literal_kmp_index<Ch, c, I>)
    {
        first = kmp_literal::forward(first, last, i, begin, I());
        while (true) {
            if (first == last)
                return i = 0, last;
            if (*first == c)
                return ((i == sizeof...(cs) + 1) ? (++begin, i) : ++i), ++first;
            first = kmp_literal::backward
                (first, last, i, begin, glgl_string_literal_kmp_index<Ch, c, I>());
        }
    }

    template <typename It, Ch c> static
    It forward(It first, It last, size_t &i, It &begin,
               glgl_string_literal_kmp_index<Ch, c, void>)
    {
        for (size_t n = 0; first != last; ++first, ++n) {
            if (n == sizeof...(cs) + 1)
                i = sizeof...(cs) + 1;
            if (n >= sizeof...(cs) + 1)
                ++begin;
            if (*first == c) {
                if (i == 0)
                    i = n;
                return ++first;
            }
        }
        return i = 0, last;
    }
};

template <typename T, size_t... Is> constexpr
auto glgl_string_literal_kmp_make(T, index_holder<Is...>)
    -> kmp_literal<typename decay<decltype(*(T::data()))>::type,
                   T::data()[Is]...>
{
    return kmp_literal<typename decay<decltype(*(T::data()))>::type,
                       T::data()[Is]...>();
}

#define GLGL_KMP_SEARCHER_LITERAL(str)                                  \
    ::glglT::glgl_string_literal_kmp_make([](){ struct { \
                static constexpr decltype(str) data() { return str; } \
    } s; return s; }(), \
        typename ::glglT::make_index<sizeof(str) / sizeof(*(str)) - 1>::type())

} // namespace glglT

#endif // GLGL_LITERAL_TEMPLATE_H

#ifndef GLGL_ITERATOR_ALGO_H
#define GLGL_ITERATOR_ALGO_H

#include "glgl_iterator_traits.h"
#include "glgl_utility_function.h"

namespace glglT {

template <typename It, typename Diff> inline
void advance(It &i, Diff n, random_access_iterator_tag)
    noexcept(noexcept(i += n))
{
    i += n;
}

template <typename It, typename Diff>
void advance(It &i, Diff n, input_iterator_tag)
    noexcept(noexcept(++i) && noexcept(--n) && noexcept(n > 0))
{
    if (n > 0)
        do ++i; while (--n);
}

template <typename It, typename Diff>
void advance(It &i, Diff n, bidirectional_iterator_tag)
    noexcept(noexcept(++i) && noexcept(--i) &&
             noexcept(n--) && noexcept(n++) && noexcept(n > 0))
{
    if (n > 0)
        while (n--)
            ++i;
    else
        while (n++)
            --i;
}

template <typename It, typename Diff> inline
void advance(It &i, Diff n)
    noexcept(noexcept
             (advance
              (i, n, typename iterator_traits<It>::iterator_category())))
{
    glglT::advance(i, n, typename iterator_traits<It>::iterator_category());
}

template <typename It> inline typename iterator_traits<It>::difference_type
distance(It f, It l, random_access_iterator_tag) noexcept(noexcept(l - f))
{
    return l - f;
}

template <typename It> typename iterator_traits<It>::difference_type
distance(It f, It l, input_iterator_tag)
    noexcept(noexcept(++f) &&
             noexcept(typename iterator_traits<It>::difference_type(0)) &&
             noexcept(++glglT::declval<typename iterator_traits<It>::difference_type &>()))
{
    typename iterator_traits<It>::difference_type n = 0;
    for (; f != l; ++f, ++n);
    return n;
}

template <typename It> inline typename iterator_traits<It>::difference_type
distance(It f, It l)
    noexcept(noexcept
             (distance
              (f, l, typename iterator_traits<It>::iterator_category())))
{
    return glglT::distance(f, l, typename iterator_traits<It>::iterator_category());
}

template <typename It> inline
It next(It i, typename iterator_traits<It>::difference_type n = 1)
    noexcept(noexcept(advance(i, n)))
{
    glglT::advance(i, n);
    return i;
}

template <typename It> inline
It prev(It i, typename iterator_traits<It>::difference_type n = 1)
    noexcept(noexcept(advance(i, -n)))
{
    glglT::advance(i, -n);
    return i;
}

template <typename C> inline
auto begin(C &c) noexcept(noexcept(c.begin())) -> decltype(c.begin())
{
    return c.begin();
}

template<typename T, size_t N> inline
T *begin(T (& arr)[N]) noexcept { return arr; }

template <typename C> inline
auto end(C &c) noexcept(noexcept(c.end())) -> decltype(c.end())
{
    return c.end();
}

template<typename T, size_t N> inline
T *end(T (& arr)[N]) noexcept { return arr + N; }

} // namespace glglT

#endif // GLGL_ITERATOR_ALGO_H

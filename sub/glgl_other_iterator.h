#ifndef GLGL_OTHER_ITERATOR_H
#define GLGL_OTHER_ITERATOR_H

#include "glgl_utility_function.h"
#include "glgl_iterator_algo.h"

namespace glglT {

template <typename It>
class normal_iterator: public iterator<
    typename iterator_traits<It>::iterator_category,
    typename iterator_traits<It>::value_type,
    typename iterator_traits<It>::difference_type,
    typename iterator_traits<It>::pointer,
    typename iterator_traits<It>::reference> {
protected:
    typedef iterator<
        typename iterator_traits<It>::iterator_category,
        typename iterator_traits<It>::value_type,
        typename iterator_traits<It>::difference_type,
        typename iterator_traits<It>::pointer,
        typename iterator_traits<It>::reference> Base;
    It current;
public:
    typedef It iterator_type;
    using typename Base::difference_type;
    using typename Base::pointer;
    using typename Base::reference;
    normal_iterator() noexcept(noexcept(It())): current() {}
    explicit normal_iterator(It it) noexcept(noexcept(It(it))): current(it) {}

    template <typename Ot>
    normal_iterator(const normal_iterator<Ot> &rit)
        noexcept(noexcept(It(rit.base()))): current(rit.base()) {}

    template <typename Ot>
    normal_iterator &operator=(const normal_iterator<Ot> &rit)
        noexcept(noexcept(current = rit.base()))
    {
        current = rit.base();
        return *this;
    }

    It base() const noexcept(noexcept(It(current))) { return current; }

    reference operator*() const noexcept(noexcept(It(current)))
    {
        return *current;
    }

    pointer operator->() const noexcept(noexcept(operator*()))
    {
        return glglT::addressof(this->operator*());
    }

    auto operator[](difference_type n) const noexcept(noexcept(current[n]))
        -> decltype(current[n])
    {
        return current[n];
    }

    normal_iterator &operator++() noexcept(noexcept(++current))
    {
        ++current;
        return *this;
    }

    normal_iterator &operator--() noexcept(noexcept(--current))
    {
        --current;
        return *this;
    }

    normal_iterator operator++(int)
        noexcept(noexcept(It(current)) && noexcept(++current))
    {
        normal_iterator tmp(current);
        ++current;
        return tmp;
    }

    normal_iterator operator--(int)
        noexcept(noexcept(It(current)) && noexcept(--current))
    {
        normal_iterator tmp(current);
        --current;
        return tmp;
    }

    normal_iterator operator+(difference_type n) const
        noexcept(noexcept(It(current)) && noexcept(current + n))
    {
        return normal_iterator(current + n);
    }

    normal_iterator operator-(difference_type n) const
        noexcept(noexcept(It(current)) && noexcept(current - n))
    {
        return normal_iterator(current - n);
    }

    normal_iterator &operator+=(difference_type n)
        noexcept(noexcept(current += n))
    {
        current += n;
        return *this;
    }

    normal_iterator &operator-=(difference_type n)
        noexcept(noexcept(current -= n))
    {
        current -= n;
        return *this;
    }
};

template <typename It1, typename It2> inline
bool operator==(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() == r2.base()))
{
    return r1.base() == r2.base();
}

template <typename It1, typename It2> inline
bool operator<=(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() <= r2.base()))
{
    return r1.base() <= r2.base();
}

template <typename It1, typename It2> inline
bool operator!=(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() != r2.base()))
{
    return r1.base() != r2.base();
}

template <typename It1, typename It2> inline
bool operator>(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() < r2.base()))
{
    return r1.base() < r2.base();
}

template <typename It1, typename It2> inline
bool operator>=(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() <= r2.base()))
{
    return r1.base() <= r2.base();
}

template <typename It1, typename It2> inline
bool operator<(const normal_iterator<It1> &r1, const normal_iterator<It2> &r2)
    noexcept(noexcept(r1.base() < r2.base()))
{
    return r1.base() < r2.base();
}

template <typename It> inline normal_iterator<It>
operator+(typename normal_iterator<It>::difference_type n,
          const normal_iterator<It> &r) noexcept(noexcept(r + n))
{
    return r + n;
}

template <typename It1, typename It2> inline
auto operator-(const normal_iterator<It1>& r1,
               const normal_iterator<It2>& r2)
    noexcept(noexcept(r1.base() - r2.base())) -> decltype(r1.base() - r2.base())
{
    return r1.base() - r2.base();
}

template <typename It>
class reverse_iterator: public iterator<
    typename iterator_traits<It>::iterator_category,
    typename iterator_traits<It>::value_type,
    typename iterator_traits<It>::difference_type,
    typename iterator_traits<It>::pointer,
    typename iterator_traits<It>::reference> {
protected:
    typedef iterator<
        typename iterator_traits<It>::iterator_category,
        typename iterator_traits<It>::value_type,
        typename iterator_traits<It>::difference_type,
        typename iterator_traits<It>::pointer,
        typename iterator_traits<It>::reference> Base;
    It current;
public:
    typedef It iterator_type;
    using typename Base::difference_type;
    using typename Base::pointer;
    using typename Base::reference;
    reverse_iterator() noexcept(noexcept(It())): current() {}
    explicit reverse_iterator(It it) noexcept(noexcept(It(it))): current(it) {}

    template <typename Ot>
    reverse_iterator(const reverse_iterator<Ot> &rit)
        noexcept(noexcept(It(rit.base()))): current(rit.base()) {}

    template <typename Ot>
    reverse_iterator &operator=(const reverse_iterator<Ot> &rit)
        noexcept(noexcept(current = rit.base()))
    {
        current = rit.base();
        return *this;
    }

    It base() const noexcept(noexcept(It(current))) { return current; }

    reference operator*() const
        noexcept(noexcept(It(current)) && noexcept(*--glglT::declval<It &>()))
    {
        It tmp(current);
        return *--tmp;
    }

    pointer operator->() const
        noexcept(noexcept(It(current)) && noexcept(*--glglT::declval<It &>()))
    {
        return glglT::addressof(this->operator*());
    }

    auto operator[](difference_type n) const
        noexcept(noexcept(current[- n - 1]))
        -> decltype(*current)
    {
        return current[- n - 1];
    }

    reverse_iterator &operator++() noexcept(noexcept(--current))
    {
        --current;
        return *this;
    }

    reverse_iterator &operator--() noexcept(noexcept(++current))
    {
        ++current;
        return *this;
    }

    reverse_iterator operator++(int)
        noexcept(noexcept(It(current)) && noexcept(--current))
    {
        reverse_iterator tmp(current);
        --current;
        return tmp;
    }

    reverse_iterator operator--(int)
        noexcept(noexcept(It(current)) && noexcept(++current))
    {
        reverse_iterator tmp(current);
        ++current;
        return tmp;
    }

    reverse_iterator operator+(difference_type n) const
        noexcept(noexcept(It(current)) && noexcept(current - n))
    {
        return reverse_iterator(current - n);
    }

    reverse_iterator operator-(difference_type n) const
        noexcept(noexcept(It(current)) && noexcept(current + n))
    {
        return reverse_iterator(current + n);
    }

    reverse_iterator &operator+=(difference_type n)
        noexcept(noexcept(current -= n))
    {
        current -= n;
        return *this;
    }

    reverse_iterator &operator-=(difference_type n)
        noexcept(noexcept(current += n))
    {
        current += n;
        return *this;
    }
};

template <typename It1, typename It2> inline
bool operator==(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() == r2.base()))
{
    return r1.base() == r2.base();
}

template <typename It1, typename It2> inline
bool operator<=(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() >= r2.base()))
{
    return r1.base() >= r2.base();
}

template <typename It1, typename It2> inline
bool operator!=(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() != r2.base()))
{
    return r1.base() != r2.base();
}

template <typename It1, typename It2> inline
bool operator>(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() < r2.base()))
{
    return r1.base() < r2.base();
}

template <typename It1, typename It2> inline
bool operator>=(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() <= r2.base()))
{
    return r1.base() <= r2.base();
}

template <typename It1, typename It2> inline
bool operator<(const reverse_iterator<It1> &r1,
                const reverse_iterator<It2> &r2)
    noexcept(noexcept(r1.base() > r2.base()))
{
    return r1.base() > r2.base();
}

template <typename It> inline reverse_iterator<It>
operator+(typename reverse_iterator<It>::difference_type n,
          const reverse_iterator<It> &r) noexcept(noexcept(r + n))
{
    return r + n;
}

template <typename It1, typename It2> inline
auto operator-(const reverse_iterator<It1>& r1,
               const reverse_iterator<It2>& r2)
    noexcept(noexcept(r2.base() - r1.base())) -> decltype(r2.base() - r1.base())
{
    return r2.base() - r1.base();
}

template <typename It>
class move_iterator {
    It current;
public:
    typedef It iterator_type;
    typedef typename iterator_traits<It>::difference_type difference_type;
    typedef It pointer;
    typedef typename iterator_traits<It>::value_type value_type;
    typedef typename iterator_traits<It>::iterator_category iterator_category;
    typedef value_type && reference;

    move_iterator() noexcept(noexcept(It())): current() {}
    explicit move_iterator(It it) noexcept(noexcept(It(it))):
        current(it) {}

    template <typename Ot>
    move_iterator(const move_iterator<Ot> &mit)
        noexcept(noexcept(It(mit.current))): current(mit.current) {}

    template <typename Ot>
    move_iterator &operator=(const move_iterator<Ot> &mit)
        noexcept(noexcept(current = mit.current))
    {
        current = mit.current;
        return *this;
    }

    It base() const noexcept(noexcept(It(current))) { return current; }
    reference operator*() const noexcept(noexcept(*current))
        { return glglT::move(*current); }
    It operator->() const noexcept(noexcept(It(current))) { return current; }

    auto operator[](difference_type n) const noexcept(noexcept(current[n]))
        -> decltype(glglT::move(current[n]))
    {
        return glglT::move(current[n]);
    }

    move_iterator &operator++() noexcept(noexcept(++current))
    {
        ++current;
        return *this;
    }

    move_iterator &operator--() noexcept(noexcept(--current))
    {
        --current;
        return *this;
    }

    move_iterator operator++(int)
        noexcept(noexcept(++current) && noexcept(It(current)))
    {
        move_iterator tmp(current);
        ++current;
        return tmp;
    }

    move_iterator operator--(int)
        noexcept(noexcept(--current) && noexcept(It(current)))
    {
        move_iterator tmp(current);
        --current;
        return tmp;
    }

    move_iterator operator+(difference_type n)
        noexcept(noexcept(current + n) && noexcept(It(current)))
    {
        return move_iterator(current + n);
    }

    move_iterator operator-(difference_type n)
        noexcept(noexcept(current - n) && noexcept(It(current)))
    {
        return move_iterator(current - n);
    }

    move_iterator &operator+=(difference_type n)
        noexcept(noexcept(current += n))
    {
        current += n;
        return *this;
    }

    move_iterator &operator-=(difference_type n)
        noexcept(noexcept(current -= n))
    {
        current -= n;
        return *this;
    }
};

template <typename It> inline
move_iterator<It> make_move_iterator(const It &i) noexcept(noexcept(It(i)))
{
    return move_iterator<It>(i);
}

template <typename It1, typename It2> inline
bool operator==(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() == r1.base()))
{
    return r1.base() == r1.base();
}

template <typename It1, typename It2> inline
bool operator>=(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() >= r1.base()))
{
    return r1.base() >= r1.base();
}

template <typename It1, typename It2> inline
bool operator!=(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() != r1.base()))
{
    return r1.base() != r1.base();
}

template <typename It1, typename It2> inline
bool operator<(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() < r1.base()))
{
    return r1.base() < r1.base();
}

template <typename It1, typename It2> inline
bool operator<=(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() <= r1.base()))
{
    return r1.base() <= r1.base();
}

template <typename It1, typename It2> inline
bool operator>(const move_iterator<It1> &r1, const move_iterator<It2> &r2)
    noexcept(noexcept(r1.base() > r1.base()))
{
    return r1.base() > r1.base();
}

template <typename It> inline move_iterator<It>
operator+(typename move_iterator<It>::difference_type n,
          const move_iterator<It> &r) noexcept(noexcept(r + n))
{
    return r + n;
}

template <typename It1, typename It2> inline
auto operator-(const move_iterator<It1>& r1, const move_iterator<It2>& r2)
    noexcept(noexcept(r1.base() - r2.base())) -> decltype(r1.base() - r2.base())
{
    return r1.base() - r2.base();
}

template <typename C>
class back_insert_iterator:
    public iterator<output_iterator_tag, void, void, void, void> {
protected:
    C *container;
public:
    typedef C container_type;

    explicit back_insert_iterator(C &c) noexcept: container(glglT::addressof(c)) {}

    back_insert_iterator &operator=(const typename C::value_type &v)
    {
        container->push_back(v);
        return *this;
    }

    back_insert_iterator &operator=(typename C::value_type &&v)
    {
        container->push_back(glglT::move(v));
        return *this;
    }

    back_insert_iterator &operator*() noexcept { return *this; }
    back_insert_iterator &operator++() noexcept { return *this; }
    back_insert_iterator &operator++(int) noexcept { return *this; }
};

template <typename C> inline
back_insert_iterator<C> back_inserter(C &c) noexcept
{
    return back_insert_iterator<C>(c);
}

template <typename C>
class front_insert_iterator:
    public iterator<output_iterator_tag, void, void, void, void> {
protected:
    C *container;
public:
    typedef C container_type;

    explicit front_insert_iterator(C &c) noexcept: container(glglT::addressof(c)) {}

    front_insert_iterator &operator=(const typename C::value_type &v)
    {
        container->push_front(v);
        return *this;
    }

    front_insert_iterator &operator=(typename C::value_type &&v)
    {
        container->push_front(glglT::move(v));
        return *this;
    }

    front_insert_iterator &operator*() noexcept { return *this; }
    front_insert_iterator &operator++() noexcept { return *this; }
    front_insert_iterator &operator++(int) noexcept { return *this; }
};

template <typename C> inline
front_insert_iterator<C> front_inserter(C &c) noexcept
{
    return front_insert_iterator<C>(c);
}

template <typename C>
class insert_iterator:
    public iterator<output_iterator_tag, void, void, void, void> {
protected:
    C *container;
    typename C::iterator iter;
public:
    typedef C container_type;

    explicit insert_iterator(C &c, typename C::iterator i)
        noexcept(noexcept(typename C::iterator(i))):
            container(glglT::addressof(c)), iter(i) {}

    insert_iterator &operator=(const typename C::value_type &v)
    {
        iter = container->insert(iter, v);
        ++iter;
        return *this;
    }

    insert_iterator &operator=(typename C::value_type &&v)
    {
        iter = container->insert(iter, glglT::move(v));
        ++iter;
        return *this;
    }

    insert_iterator &operator*() noexcept { return *this; }
    insert_iterator &operator++() noexcept { return *this; }
    insert_iterator &operator++(int) noexcept { return *this; }
};

template <typename C> inline
insert_iterator<C> inserter(C &c, typename C::iterator i)
    noexcept(noexcept(typename C::iterator(i)))
{
    return insert_iterator<C>(c, i);
}

} // namespace glglT

#endif // GLGL_OTHER_ITERATOR_H

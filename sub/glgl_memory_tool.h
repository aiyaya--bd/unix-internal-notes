#ifndef GLGL_MEMORY_TOOL_H
#define GLGL_MEMORY_TOOL_H

#include <type_traits>
#include <cstring>
#include "glgl_allocator_traits.h"
#include "glgl_iterator_algo.h"
#include "glgl_allocator.h"
#include "glgl_pair.h"

namespace glglT {

template <typename It> inline
It real_last(It first, It last, input_iterator_tag) { return last; }

template <typename It> inline
It real_last(It first, It last, random_access_iterator_tag)
{
    return last < first ? first : last;
}

template <typename It> inline
It real_last(It first, It last)
{
    return glglT::real_last(first, last,
                            typename iterator_traits<It>::iterator_category());
}

template <typename T, bool, bool>
struct uninitialized_helper {
    template <typename It1, typename It2> static
    It2 uninitialized_copy(It1 b, It1 e, It2 b2)
    {
        auto ob = b2;
        try {
            for (; b != e; ++b2, ++b)
                ::new (glglT::addressof(*b2)) T (*b);
            return b2;
        } catch (...) {
            for (; b2 != ob; ++ob)
                ob->~T();
            throw;
        }
        return b2;
    }

    template <typename It1, typename It2> static
    It2 uninitialized_copy_n(It1 b, size_t n, It2 b2)
    {
        auto ob = b2;
        try {
            for (; n-- != 0; ++b2, ++b)
                ::new (glglT::addressof(*b2)) T (*b);
            return b2;
        } catch (...) {
            for (; b2 != ob; ++ob)
                ob->~T();
            throw;
        }
        return b2;
    }

    template <typename It> static
    void uninitialized_fill(It b, It e, const T &t)
    {
        auto ob = b;
        try {
            for (; b != e; ++b)
                ::new (glglT::addressof(*b)) T (t);
        } catch (...) {
            for (; b != ob; ++ob)
                ob->~T();
            throw;
        }
    }

    template <typename It> static
    void uninitialized_fill_n(It b, size_t n, const T &t)
    {
        auto ob = b;
        try {
            for (; n-- != 0; ++b)
                ::new (glglT::addressof(*b)) T (t);
        } catch (...) {
            for (; b != ob; ++ob)
                ob->~T();
            throw;
        }
    }
};

template <typename T>
struct uninitialized_helper<T, true, false> {
    template <typename It1, typename It2> static
    It2 uninitialized_copy(It1 b, It1 e, It2 b2) noexcept
    {
        const auto r = std::memcpy(b2, b, sizeof(T) * glglT::distance(b, e));
        return static_cast<It2>(r);
    }

    template <typename It1, typename It2> static
    It2 uninitialized_copy_n(It1 b, size_t n, It2 b2) noexcept
    {
        const auto r = std::memcpy(b2, b, sizeof(T) * n);
        return static_cast<It2>(r);
    }

    template <typename It> static
    void uninitialized_fill(It b, It e, const T &t) noexcept
    {
        uninitialized_fill_n(b, e - b, t);
    }

    template <typename It> static
    void uninitialized_fill_n(It b, size_t n, const T &t) noexcept
    {
        while (n-- != 0)
            *b++ = t;
    }
};

template <typename T>
struct uninitialized_helper<T, true, true> {
    template <typename It1, typename It2> static
    It2 uninitialized_copy(It1 b, It1 e, It2 b2) noexcept
    {
        const auto r = std::memcpy(b2, b, glglT::distance(b, e));
        return static_cast<It2>(r);
    }

    template <typename It1, typename It2> static
    It2 uninitialized_copy_n(It1 b, size_t n, It2 b2) noexcept
    {
        const auto r = std::memcpy(b2, b, n);
        return static_cast<It2>(r);
    }

    template <typename It> static
    void uninitialized_fill(It b, It e, const T &t) noexcept
    {
        std::memset(b, t, glglT::distance(b, e));
    }

    template <typename It> static
    void uninitialized_fill_n(It b, size_t n, const T &t) noexcept
    {
        std::memset(b, t, n);
    }
};

template <typename It1, typename It2> inline
It2 uninitialized_copy(It1 b, It1 e, It2 b2)
{
    e = real_last(b, e);
    typedef typename remove_reference<decltype(*b2)>::type type;
    typedef typename remove_reference<decltype(*b)>::type otype;
    return uninitialized_helper<type, is_same<type, otype>::value &&
                                      is_pointer<It1>::value &&
                                      is_pointer<It2>::value &&
                                      std::is_trivially_copyable<type>::value,
                                      sizeof(type) == 1>::
        uninitialized_copy(b, e, b2);
}

template <typename It1, typename SZ, typename It2> inline
It2 uninitialized_copy_n(It1 b, SZ n, It2 b2)
{
    n = n < 0 ? 0 : n;
    typedef typename remove_reference<decltype(*b2)>::type type;
    typedef typename remove_reference<decltype(*b)>::type otype;
    return uninitialized_helper<type, is_same<type, otype>::value &&
                                      is_pointer<It1>::value &&
                                      is_pointer<It2>::value &&
                                      std::is_trivially_copyable<type>::value,
                                      sizeof(type) == 1>::
        uninitialized_copy_n(b, n, b2);
}

template <typename It, typename T> inline
void uninitialized_fill(It b, It e, const T &t)
{
    e = real_last(b, e);
    typedef typename remove_reference<decltype(*b)>::type type;
    typedef typename remove_reference<T>::type otype;
    uninitialized_helper<type, is_same<type, otype>::value &&
                               is_pointer<It>::value &&
                               std::is_trivially_copyable<type>::value,
                               sizeof(type) == 1>::
        uninitialized_fill(b, e, t);
}

template <typename It, typename SZ, typename T> inline
void uninitialized_fill_n(It b, SZ n, const T &t)
{
    n = n < 0 ? 0 : n;
    typedef typename remove_reference<decltype(*b)>::type type;
    typedef typename remove_reference<T>::type otype;
    uninitialized_helper<type, is_same<type, otype>::value &&
                               is_pointer<It>::value &&
                               std::is_trivially_copyable<type>::value,
                               sizeof(type) == 1>::
        uninitialized_fill_n(b, n, t);
}

template <typename It1, typename SZ, typename It2>
void uninitialized_move_n(It1 b, SZ n, It2 b2)
{
    typedef typename remove_reference<decltype(*b2)>::type T;
    auto ob = b2;
    auto _b = b;
    try {
        for (; n-- != 0; ++b2, ++b)
            ::new (glglT::addressof(*b2)) T (glglT::move(*b));
    } catch (...) {
        for (; b2 != ob; ++ob) {
            *_b++ = glglT::move(*ob);
            ob->~T();
        }
        throw;
    }
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value,
                                        T>::type> inline
void uninitialized_move_n(T *b, size_t n, T *b2)
{
    uninitialized_helper<T, true, sizeof(T) == 1>::uninitialized_copy_n(b, n, b2);
}

template <typename T,
          typename = typename enable_if<std::is_trivial<T>::value, T>::type>
inline void uninitialized_emplace_n(T *b, size_t n)
{
    std::memset(b, 0, n * sizeof(T));
}

template <typename It, typename... Args>
void uninitialized_emplace_n(It b, size_t n, Args&&... args)
{
    typedef typename decay<decltype(*b)>::type T;
    const auto ob = b;
    try {
        for (; n-- != 0; ++b)
            ::new (glglT::addressof(*b)) T (glglT::forward<Args>(args)...);
    } catch (...) {
        for (--b; b != ob;)
            (--b)->~T();
        throw;
    }
}

template <typename T,
          typename = typename enable_if<std::is_trivial<T>::value, T>::type>
inline void pointer_emplace_n(T *b, size_t n)
{
    std::memset(b, 0, n * sizeof(T));
}

template <typename T, typename... Args>
void pointer_emplace_n(T *b, size_t n, Args&&... args)
{
    for (; n-- != 0; ++b)
        *b = T{};
}

template <typename It, typename SZ>
void uninitialized_destroy(It b, SZ n) noexcept
{
    typedef typename decay<decltype(*b)>::type T;
    while (n-- != 0)
        b++->~T();
}

template <typename Pt, typename It, typename SZ>
void uninitialized_destroy_move(Pt b, It ob, SZ n) noexcept
{
    typedef typename decay<decltype(*b)>::type T;
    while (n-- != 0) {
        *ob++ = glglT::move(*b);
        b++->~T();
    }
}

template <typename It,
          typename T = typename decay<decltype(*glglT::declval<It>())>::type,
          typename = typename enable_if<std::is_trivial<T>::value, T>::type>
inline void uninitialized_destroy(It , size_t) noexcept {}
template <typename Pt, typename It,
          typename T = typename decay<decltype(*glglT::declval<It>())>::type,
          typename = typename enable_if<std::is_trivial<T>::value, T>::type>
inline void uninitialized_destroy_move(Pt b, It ob, size_t n) noexcept
{
    std::memcpy(ob, b, sizeof(T) * n);
}

template <typename Alloc, typename Alloc_Traits>
struct uninitialized_with_alloc {
    template <typename It, typename Pt> static
    void uninitialized_copy_n(Alloc &a, It b, typename Alloc_Traits::size_type n, Pt b2)
    {
        auto ob = b2;
        try {
            for (; n-- != 0; ++b2, ++b)
                Alloc_Traits::construct(a, glglT::addressof(*b2), *b);
        } catch (...) {
            for (; b2 != ob; ++ob)
                Alloc_Traits::destroy(a, glglT::addressof(*ob));
            throw;
        }
    }

    template <typename It, typename Pt> static
    void uninitialized_move_n(Alloc &a, It b, typename Alloc_Traits::size_type n, Pt b2)
    {
        auto ob = b2;
        auto _b = b;
        try {
            for (; n-- != 0; ++b2, ++b)
                Alloc_Traits::construct(a, glglT::addressof(*b2), glglT::move(*b));
        } catch (...) {
            for (; b2 != ob; ++ob) {
                *_b++ = glglT::move(*ob);
                Alloc_Traits::destroy(a, glglT::addressof(*ob));
            }
            throw;
        }
    }

    template <typename Pt, typename... Args> static
    void uninitialized_fill_n(Alloc &a, Pt b, typename Alloc_Traits::size_type n, Args&&... args)
    {
        auto ob = b;
        try {
            for (; n-- != 0; ++b)
                Alloc_Traits::construct(a, glglT::addressof(*b), glglT::forward<Args>(args)...);
        } catch (...) {
            for (; b != ob; ++ob)
                Alloc_Traits::destroy(a, glglT::addressof(*ob));
            throw;
        }
    }

    template <typename Pt> static
    void destroy(Alloc &a, Pt b, typename Alloc_Traits::size_type n) noexcept
    {
        while (n-- != 0)
            Alloc_Traits::destroy(a, glglT::addressof(b[n]));
    }

    template <typename Pt, typename It> static
    void destroy_move(Alloc &a, Pt b, It ob, typename Alloc_Traits::size_type n) noexcept
    {
        while (n-- != 0) {
            *ob = glglT::move(b[n]);
            Alloc_Traits::destroy(a, glglT::addressof(b[n]));
        }
    }
};

template <typename T>
struct uninitialized_with_alloc<allocator<T>, allocator_traits<allocator<T>>> {
    template <typename It, typename Pt> static
    void uninitialized_copy_n(allocator<T> &, It b, size_t n, Pt b2)
    {
        glglT::uninitialized_copy_n(b, n, b2);
    }

    template <typename It, typename Pt> static
    void uninitialized_move_n(allocator<T> &, It b, size_t n, Pt b2)
    {
        glglT::uninitialized_move_n(b, n, b2);
    }

    template <typename Pt> static
    void uninitialized_fill_n(allocator<T> &, Pt b, size_t n, T &t)
    {
        glglT::uninitialized_fill_n(b, n, t);
    }

    template <typename Pt> static
    void uninitialized_fill_n(allocator<T> &, Pt b, size_t n, const T &t)
    {
        glglT::uninitialized_fill_n(b, n, t);
    }

    template <typename Pt> static
    void uninitialized_fill_n(allocator<T> &, Pt b, size_t n, T &&t)
    {
        glglT::uninitialized_fill_n(b, n, t);
    }

    template <typename Pt, typename... Args> static
    void uninitialized_fill_n(allocator<T> &, Pt b, size_t n, Args&&... args)
    {
        glglT::uninitialized_emplace_n(b, n, glglT::forward<Args>(args)...);
    }

    template <typename Pt> static
    void destroy(allocator<T> &, Pt b, size_t n) noexcept
    {
        glglT::uninitialized_destroy(b, n);
    }

    template <typename Pt, typename It> static
    void destroy_move(allocator<T> &, Pt b, It ob, size_t n) noexcept
    {
        glglT::uninitialized_destroy_move(b, ob, n);
    }
};

using std::allocator_arg_t;

using std::allocator_arg;

using std::uses_allocator;

template <typename T>
pair<T *, ptrdiff_t> get_temporary_buffer(ptrdiff_t len) noexcept
{
    for (T *p; len > 0; len >>= 1)
        if ((p = static_cast<T *>(::operator new (len * sizeof(T), std::nothrow))))
            return pair<T *, ptrdiff_t>(p, len);
    return pair<T *, ptrdiff_t>(nullptr, 0);
}

template <typename T> inline
void return_temporary_buffer(T *p) noexcept
{
    ::operator delete (p, std::nothrow);
}

template <typename It, typename T>
class raw_storage_iterator:
    public iterator<output_iterator_tag, void, void, void, void> {
    It itr;
public:
    explicit raw_storage_iterator(It it)
        noexcept(noexcept(It(it))): itr(it) {}

    raw_storage_iterator &operator=(const T &t) noexcept(noexcept(T(t)))
    {
        ::new (addressof(*itr)) T (t);
        return *this;
    }

    raw_storage_iterator &operator*() noexcept { return *this; }
    raw_storage_iterator &operator++() noexcept(noexcept(++itr))
    {
        ++itr;
        return *this;
    }

    raw_storage_iterator &operator++(int)
        noexcept(noexcept(It(itr)) && noexcept(++itr))
    {
        raw_storage_iterator tmp(itr);
        ++itr;
        return tmp;
    }
};

} // namespace glglT

#endif // GLGL_MEMORY_TOOL_H

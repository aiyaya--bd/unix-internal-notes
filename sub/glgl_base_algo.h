#ifndef GLGL_BASE_ALGO_H
#define GLGL_BASE_ALGO_H

#include <random>
#include "glgl_memory_tool.h"

namespace glglT {

template <typename It, typename Pred>
It find_if(It first, It last, Pred p)
{
    last = glglT::real_last(first, last);
    while (first != last && !(p(*first)))
        ++first;
    return first;
}

template <typename It, typename T>
It find(It first, It last, const T &t)
{
    last = glglT::real_last(first, last);
    while (first != last && *first != t)
        ++first;
    return first;
}

template <typename It, typename Pred>
It find_if_not(It first, It last, Pred p)
{
    last = glglT::real_last(first, last);
    while (first != last && p(*first))
        ++first;
    return first;
}

template <typename It, typename Pred> inline
bool all_of(It first, It last, Pred p)
{
    return glglT::find_if_not(first, last, p) == last;
}

template <typename It, typename Pred> inline
bool any_of(It first, It last, Pred p)
{
    return glglT::find_if(first, last, p) != last;
}

template <typename It, typename Pred> inline
bool none_of(It first, It last, Pred p)
{
    return glglT::find_if(first, last, p) == last;
}

template <typename It, typename Func>
Func for_each(It first, It last, Func f)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        f(*first);
    return f;
}

template <typename It, typename Pred>
auto count_if(It first, It last, Pred p)
    -> typename iterator_traits<It>::difference_type
{
    last = glglT::real_last(first, last);
    typename iterator_traits<It>::difference_type cnt = 0;
    for (; first != last; ++first)
        cnt += bool(p(*first));
    return cnt;
}

template <typename It, typename T>
auto count(It first, It last, const T &t)
    -> typename iterator_traits<It>::difference_type
{
    last = glglT::real_last(first, last);
    typename iterator_traits<It>::difference_type cnt = 0;
    for (; first != last; ++first)
        cnt += (*first == t);
    return cnt;
}

template <typename It1, typename It2, typename Pred>
pair<It1, It2> mismatch(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last && p(*first1, *first2); ++first1, ++first2);
    return pair<It1, It2>(first1, first2);
}

template <typename It1, typename It2>
pair<It1, It2> mismatch(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last && *first1 == *first2; ++first1, ++first2);
    return pair<It1, It2>(first1, first2);
}

template <typename It1, typename It2, typename Pred>
bool equal(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        if (!p(*first1, *first2))
            return false;
    return true;
}

template <typename It1, typename It2>
bool equal(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        if (*first1 != *first2)
            return false;
    return true;
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline bool equal(T *first1, T *last, T *first2)
{
    last = glglT::real_last(first1, last);
    return std::memcmp(first1, first2, sizeof(T) * (last - first1)) == 0;
}

template <typename It1, typename It2, typename Pred>
It1 search(It1 first, It1 last, It2 s_first, It2 s_last, Pred p)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    for (It1 it1 = first; ; ++first) {
        for (It2 it2 = s_first; ; ++it1, ++it2) {
            if (it2 == s_last)
                return first;
            if (it1 == last)
                return last;
            if (!p(*it1, *it2))
                break;
        }
    }
}

template <typename It1, typename It2>
It1 search(It1 first, It1 last, It2 s_first, It2 s_last)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    for (It1 it1 = first; ; ++first) {
        for (It2 it2 = s_first; ; ++it1, ++it2) {
            if (it2 == s_last)
                return first;
            if (it1 == last)
                return last;
            if (*it1 != *it2)
                break;
        }
    }
}

template <typename It1, typename It2, typename Pred>
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last, Pred p,
             forward_iterator_tag, forward_iterator_tag)
{
    if (s_first == s_last)
        return last;
    for (It1 res = last; ; ++first) {
        It1 new_res = glglT::search(first, last, s_first, s_last, p);
        if (new_res == last)
            return res;
        res = new_res;
        first = new_res;
    }
}

template <typename It1, typename It2, typename Pred>
It1 rsearch(It1 rfirst, It1 rlast, It2 rs_first, It2 rs_last, Pred p)
{
    --rs_first;
    --rs_last;
    --rfirst;
    for (It1 it1 = rfirst; ; --rfirst) {
        for (It2 it2 = rs_first; ; --it1, --it2) {
            if (it2 == rs_last)
                return rfirst;
            if (it1 == rlast)
                return rlast;
            if (!p(*it1, *it2))
                break;
        }
    }
}

template <typename It1, typename It2, typename Pred> inline
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last, Pred p,
             bidirectional_iterator_tag, bidirectional_iterator_tag)
{
    It1 res = glglT::rsearch(last, --first, s_last, s_first, p);
    if (res == first)
        return last;
    glglT::advance(res, 1 - glglT::distance(s_first, s_last));
    return res;
}

template <typename It1, typename It2, typename Pred> inline
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last, Pred p)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    return glglT::find_end(first, last, s_first, s_last, p,
                    typename iterator_traits<It1>::iterator_category(),
                    typename iterator_traits<It2>::iterator_category());
}

template <typename It1, typename It2>
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last,
             forward_iterator_tag, forward_iterator_tag)
{
    if (s_first == s_last)
        return last;
    for (It1 res = last; ; ++first) {
        It1 new_res = glglT::search(first, last, s_first, s_last);
        if (new_res == last)
            return res;
        res = new_res;
        first = new_res;
    }
}

template <typename It1, typename It2>
It1 rsearch(It1 rfirst, It1 rlast, It2 rs_first, It2 rs_last)
{
    --rs_first;
    --rs_last;
    --rfirst;
    for (It1 it1 = rfirst; ; --rfirst) {
        for (It2 it2 = rs_first; ; --it1, --it2) {
            if (it2 == rs_last)
                return rfirst;
            if (it1 == rlast)
                return rlast;
            if (*it1 != *it2)
                break;
        }
    }
}

template <typename It1, typename It2, typename Pred> inline
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last,
             bidirectional_iterator_tag, bidirectional_iterator_tag)
{
    It1 res = glglT::rsearch(last, --first, s_last, s_first);
    if (res == first)
        return last;
    glglT::advance(res, 1 - glglT::distance(s_first, s_last));
    return res;
}

template <typename It1, typename It2> inline
It1 find_end(It1 first, It1 last, It2 s_first, It2 s_last)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    return glglT::find_end(first, last, s_first, s_last,
                    typename iterator_traits<It1>::iterator_category(),
                    typename iterator_traits<It2>::iterator_category());
}

template <typename It1, typename It2, typename Pred>
It1 find_first_of(It1 first, It1 last, It2 s_first, It2 s_last, Pred p)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    for (; first != last; ++first)
        for (It2 it = s_first; it != s_last; ++it)
            if (p(*first, *it))
                return first;
    return last;
}

template <typename It1, typename It2>
It1 find_first_of(It1 first, It1 last, It2 s_first, It2 s_last)
{
    last = glglT::real_last(first, last);
    s_last = glglT::real_last(s_first, s_last);
    for (; first != last; ++first)
        for (It2 it = s_first; it != s_last; ++it)
            if (*first == *it)
                return first;
    return last;
}

template <typename It, typename Pred>
It adjacent_find(It first, It last, Pred p)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return last;
    for (It next = first; ++next != last; first = next)
        if (p(*first, *next))
            return first;
    return last;
}

template <typename It>
It adjacent_find(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return last;
    for (It next = first; ++next != last; first = next)
        if (*first == *next)
            return first;
    return last;
}

template <typename It, typename SZ, typename T, typename Pred>
It search_n(It first, It last, SZ n, const T &t, Pred p)
{
    if (n <= 0)
        return first;
    last = glglT::real_last(first, last);
    for (; first != last; ++first) {
        if (p(*first, t)) {
            SZ i = 1;
            It c = first;
            do {
                if (i == n)
                    return c;
                ++first;
                if (first == last)
                    return last;
                ++i;
            } while (p(*first, t));
        }
    }
    return last;
}

template <typename It, typename SZ, typename T>
It search_n(It first, It last, SZ n, const T &t)
{
    if (n <= 0)
        return first;
    last = glglT::real_last(first, last);
    for (; first != last; ++first) {
        if (*first == t) {
            SZ i = 1;
            It c = first;
            do {
                if (i == n)
                    return c;
                ++first;
                if (first == last)
                    return last;
                ++i;
            } while (*first == t);
        }
    }
    return last;
}

template <typename It1, typename It2>
It2 copy(It1 first1, It1 last, It2 first2, random_access_iterator_tag)
{
    for (auto n = last - first1; n-- != 0; ++first1, ++first2)
        *first2 = *first1;
    return first2;
}

template <typename It1, typename It2>
It2 copy(It1 first1, It1 last, It2 first2, input_iterator_tag)
{
    for (; first1 != last; ++first1, ++first2)
        *first2 = *first1;
    return first2;
}

template <typename It1, typename It2> inline
It2 copy(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    return glglT::copy(first1, last, first2,
                typename iterator_traits<It1>::iterator_category());
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *copy(T *first1, T *last, T *first2)
{
    last = glglT::real_last(first1, last);
    const auto n = last - first1;
    const auto res = first2 + n;
    if (first2 <= first1 && res > first1)
        std::memmove(first2, first1, n * sizeof(T));
    else
        std::memcpy(first2, first1, n * sizeof(T));
    return res;
}

template <typename It1, typename It2, typename Pred>
It2 copy_if(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1) {
        if (p(*first1)) {
            *first2 = *first1;
            ++first2;
        }
    }
    return first2;
}

template <typename It1, typename SZ, typename It2>
It2 copy_n(It1 first1, SZ n, It2 first2)
{
    if (n > 0) {
        do {
            *first2 = *first1;
            ++first1;
            ++first2;
        } while (--n != 0);
    }
    return first2;
}

template <typename T, typename SZ,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *copy_n(T *first1, SZ n, T *first2)
{
    return glglT::copy(first1, first1 + n, first2);
}

template <typename It1, typename It2>
It2 copy_backward(It1 first, It1 last1, It2 last2, random_access_iterator_tag)
{
    for (auto n = last1 - first; n-- != 0;)
        *--last2 = *--last1;
    return last2;
}

template <typename It1, typename It2>
It2 copy_backward(It1 first, It1 last1, It2 last2, input_iterator_tag)
{
    while (first != last1)
        *--last2 = *--last1;
    return last2;
}

template <typename It1, typename It2> inline
It2 copy_backward(It1 first, It1 last1, It2 last2)
{
    last1 = glglT::real_last(first, last1);
    return glglT::copy_backward(first, last1, last2,
                         typename iterator_traits<It1>::iterator_category());
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *copy_backward(T *first, T *last1, T *last2)
{
    last1 = glglT::real_last(first, last1);
    const auto n = last1 - first;
    const auto res = last2 - n;
    if (last2 >= last1 && res < last1)
        std::memmove(res, first, n * sizeof(T));
    else
        std::memcpy(res, first, n * sizeof(T));
    return res;
}

template <typename It1, typename It2>
It2 move(It1 first1, It1 last, It2 first2, random_access_iterator_tag)
{
    for (auto n = last - first1; n-- != 0; ++first1, ++first2)
        *first2 = glglT::move(*first1);
    return first2;
}

template <typename It1, typename It2>
It2 move(It1 first1, It1 last, It2 first2, input_iterator_tag)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        *first2 = glglT::move(*first1);
    return first2;
}

template <typename It1, typename It2> inline
It2 move(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    return glglT::move(first1, last, first2,
                typename iterator_traits<It1>::iterator_category());
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *move(T *first1, T *last, T *first2)
{
    return glglT::copy(first1, last, first2);
}

template <typename It1, typename It2>
It2 move_backward(It1 first, It1 last1, It2 last2, random_access_iterator_tag)
{
    for (auto n = last1 - first; n-- != 0;)
        *--last2 = glglT::move(*--last1);
    return last2;
}

template <typename It1, typename It2>
It2 move_backward(It1 first, It1 last1, It2 last2, input_iterator_tag)
{
    while (first != last1)
        *--last2 = glglT::move(*--last1);
    return last2;
}

template <typename It1, typename It2> inline
It2 move_backward(It1 first, It1 last1, It2 last2)
{
    last1 = glglT::real_last(first, last1);
    return glglT::move_backward(first, last1, last2,
                         typename iterator_traits<It1>::iterator_category());
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *move_backward(T *first, T *last1, T *last2)
{
    return glglT::copy_backward(first, last1, last2);
}

template <typename It, typename T>
void fill(It first, It last, const T &t)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        *first = t;
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline void fill(T *first, T *last, const T &t)
{
    last = glglT::real_last(first, last);
    uninitialized_helper<T, true, sizeof(T) == 1>::
        uninitialized_fill(first, last, t);
}

template <typename It, typename SZ, typename T>
It fill_n(It first, SZ n, const T &t)
{
    if (n > 0) {
        do {
            *first = t;
            ++first;
        } while (--n != 0);
    }
    return first;
}

template <typename T, typename SZ,
          typename = typename enable_if<std::is_trivially_copyable<T>::value>::type>
inline T *fill_n(T *first, SZ n, const T &t)
{
    n = n < 0 ? 0 : n;
    uninitialized_helper<T, true, sizeof(T) == 1>::
        uninitialized_fill_n(first, n, t);
    return first + n;
}

template <typename It1, typename It2, typename Pred>
It2 transform(It1 first, It1 last, It2 res, Pred p)
{
    for (; first != last; ++res, ++first)
        res = p(*first);
    return res;
}

template <typename It1, typename It2, typename It3, typename Pred>
It2 transform(It1 first1, It1 last, It2 first2, It3 res, Pred p)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++res, ++first1, ++first2)
        res = p(*first1, *first2);
    return res;
}

template <typename It, typename G>
void generate(It first, It last, G g)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        *first = g();
}

template <typename It, typename SZ, typename G>
It generate_n(It first, SZ n, G g)
{
    if (n > 0) {
        do {
            *first = g();
            ++first;
        } while (--n != 0);
    }
}

template <typename It, typename T>
It remove(It first, It last, const T &t)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return first;
    while (*first != t && ++first != last);
    for (It next = first; ++next != last;) {
        if (*next != t) {
            *first = glglT::move(*next);
            ++first;
        }
    }
    return first;
}

template <typename It, typename Pred>
It remove_if(It first, It last, Pred p)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return first;
    while (!p(*first) && ++first != last);
    for (It next = first; ++next != last;) {
        if (!p(*next)) {
            *first = glglT::move(*next);
            ++first;
        }
    }
    return first;
}

template <typename It1, typename It2, typename T>
It2 remove_copy(It1 first1, It1 last, It2 first2, const T &t)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1) {
        if (*first1 != t) {
            *first2 = *first1;
            ++first2;
        }
    }
    return first2;
}

template <typename It1, typename It2, typename Pred>
It2 remove_copy_if(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1) {
        if (!p(*first1)) {
            *first2 = *first1;
            ++first2;
        }
    }
    return first2;
}

template <typename It, typename T>
void replace(It first, It last, const T &t1, const T &t2)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        if (*first == t1)
            *first = t2;
    return first;
}

template <typename It, typename Pred, typename T>
void replace(It first, It last, Pred p, const T &t2)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        if (p(*first))
            *first = t2;
    return first;
}

template <typename It1, typename It2, typename T>
It2 replace_copy(It1 first1, It1 last, It2 first2, const T t1, const T t2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        *first2 = *first1 == t1 ? t2 : *first1;
    return first2;
}

template <typename It1, typename It2, typename Pred, typename T>
It2 replace_copy_if(It1 first1, It1 last, It2 first2, Pred p, const T t2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        *first2 = p(*first1) ? t2 : *first1;
    return first2;
}

template <typename It1, typename It2> inline
void iter_swap(It1 it1, It2 it2)
{
    swap(*it1, *it2);
}

template <typename It1, typename It2>
It2 swap_ranges(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
        glglT::iter_swap(first1, first2);
    return first2;
}

template <typename It>
void reverse(It first, It last, random_access_iterator_tag)
{
    if (first == last)
        return;
    for (; first < --last; ++first)
        glglT::iter_swap(first, last);
}
template <typename It>
void reverse(It first, It last, bidirectional_iterator_tag)
{
    for (; first != last && first != --last; ++first)
        glglT::iter_swap(first, last);
}

template <typename It> inline
void reverse(It first, It last)
{
    glglT::reverse(first, last,
            typename iterator_traits<It>::iterator_category());
}

template <typename It1, typename It2>
It2 reverse_copy(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first2)
        *first2 = *--last;
    return first2;
}

template <typename It>
It rotate(It first, It middle, It last, forward_iterator_tag)
{
    if (first == middle)
        return last;
    else if (middle == last)
        return first;

    It itr = middle;
    do {
        glglT::iter_swap(first, itr);
        ++itr;
        ++first;
        if (first == middle)
            middle = itr;
    } while (itr != last);
    It res = first;

    itr = middle;
    while (itr != last) {
        iter_swap(first, itr);
        ++itr;
        ++first;
        if (first == middle)
            middle = itr;
        else if (itr == last)
            itr = middle;
    }
    return res;
}

template <typename It>
It rotate(It first, It middle, It last, bidirectional_iterator_tag)
{
    if (first == middle)
        return last;
    else if (middle == last)
        return first;

    const It mid = middle;
    It old_first = first, old_last = last, middle0 = middle;
    do {
        if (first == --middle)
            break;
        glglT::iter_swap(first, middle);
    } while (++first != middle);
    do {
        if (middle0 == --last)
            break;
        glglT::iter_swap(middle0, last);
    } while (++middle0 != last);

    while (true) {
        if (--old_last == mid) {
            It res = old_first;
            do {
                glglT::iter_swap(old_first, old_last);
            } while (++old_first != old_last && old_first != --old_last);
            return ++res;
        }
        glglT::iter_swap(old_first, old_last);
        if (++old_first == mid) {
            const It res = old_last;
            do {
                if (old_first == --old_last)
                    break;
                glglT::iter_swap(old_first, old_last);
            } while (++old_first != old_last);
            return res;
        }
    }
}

template <typename It>
It rotate(It first, It middle, It last, random_access_iterator_tag)
{
    if (first == middle)
        return last;
    else if (middle == last)
        return first;
    const It res = first + (last - middle);
    for (const auto d1 = middle - first, d2 = last - middle; ;) {
        if (d1 > 0 && d2 > 0) {
            if (d1 > d2) {
                const auto d = d1 % d2;
                do {
                    glglT::iter_swap(--middle, --last);
                } while (first != middle);
                if (d == 0)
                    return res;
                middle = first + d;
            } else {
                const auto d = d2 % d1;
                do {
                    glglT::iter_swap(first, middle);
                    ++first;
                } while (++middle != last);
                if (d == 0)
                    return res;
                middle = last - d;
            }
            continue;
        }
        break;
    }
    return res;
}

template <typename It> inline
It rotate(It first, It middle, It last)
{
    return glglT::rotate(first, middle, last,
                  typename iterator_traits<It>::iterator_category());
}

template <typename It1, typename It2> inline
It2 rotate_copy(It1 first1, It1 middle, It1 last, It2 first2)
{
    return glglT::copy(first1, middle, glglT::copy(middle, last, first2));
}

template <typename It, typename URNG>
void shuffle(It first, It last, URNG &&g)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return;
    const auto d = last - first - 1;
    std::uniform_int_distribution<decltype(last - first)> uid;
    typedef typename decltype(uid)::param_type param_type;
    for (decltype(last - first) i = 0; i != d; ++i) {
        using glglT::swap;
        swap(first[i], first[uid(g, param_type(i, d))]);
    }
}

template <typename It>
It unique(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return first;
    for (It next = first; ++next != last;)
        if (*first != *next && ++first != next)
            *first = glglT::move(*next);
    return first;
}

template <typename It, typename Pred>
It unique(It first, It last, Pred p)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return first;
    for (It next = first; ++next != last;)
        if (!p(*first, *next) && ++first != next)
            *first = glglT::move(*next);
    return first;
}

template <typename It1, typename It2>
It2 unique_copy(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    if (first1 == last)
        return first2;
    for (*first2 = *first1; ++first1 != last;) {
        if (*first1 == *first2)
            continue;
        *++first2 = *first1;
    }
    return ++first2;
}

template <typename It1, typename It2, typename Pred>
It2 unique_copy(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    if (first1 == last)
        return first2;
    for (*first2 = *first1; ++first1 != last;) {
        if (p(*first1, *first2))
            continue;
        *++first2 = *first1;
    }
    return ++first2;
}

} // namespace glglT

#endif // GLGL_BASE_ALGO_H

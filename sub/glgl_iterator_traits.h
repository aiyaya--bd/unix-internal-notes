#ifndef GLGL_ITERATOR_TRAITS_H
#define GLGL_ITERATOR_TRAITS_H

#include "glgl_basic_iterator.h"

namespace glglT {

using std::iterator_traits;

} // namespace glglT


#endif // GLGL_ITERATOR_TRAITS_H

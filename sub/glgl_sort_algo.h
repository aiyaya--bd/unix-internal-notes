#ifndef GLGL_SORT_ALGO_H
#define GLGL_SORT_ALGO_H

#include <initializer_list>
#include <cmath>
#include "glgl_base_algo.h"

namespace glglT {

template <typename It, typename Pred>
bool is_partitioned(It first, It last, Pred p)
{
    for (last = glglT::real_last(first, last); first != last; ++first) {
        if (!p(*first)) {
            for (++first; first != last; ++first)
                if (p(*first))
                    return false;
            return true;
        }
    }
    return true;
}

template <typename It, typename Pred>
It partition(It first, It last, Pred p)
{
    for (last = glglT::real_last(first, last); first != last && p(*first);)
         ++first;
    if (first != last) {
        for (It next = first; ++next != last;) {
            if (p(*next)) {
                glglT::iter_swap(first, next);
                ++first;
            }
        }
    }
    return first;
}

template <typename It, typename It1, typename It2, typename Pred>
pair<It1, It2> partition_copy(It first, It last, It1 tfirst, It2 ffirst, Pred p)
{
    for (last = glglT::real_last(first, last); first != last; ++first) {
        if (p(*first)) {
            *tfirst = *first;
            ++tfirst;
        } else {
            *ffirst = *first;
            ++ffirst;
        }
    }
    return pair<It1, It2>(tfirst, ffirst);
}

template <typename It, typename Int1, typename T, typename Int2, typename Pred>
It stable_partition(It first, It last, Int1 len, T *buf, Int2 buf_len, Pred p)
{
    if (len <= 1)
        return p(*first) ? last : first;
    It mid;
    if (len <= buf_len) {
        T *buf_beg = buf, *buf_end = buf + buf_len;
        for (It itr = first; itr != last; ++itr)
            if (p(*itr))
                *buf_beg++ = glglT::move(*itr);
            else
                *--buf_end = glglT::move(*itr);
        It itr = first;
        for (T *ptr = buf; ptr != buf_beg; ++ptr, ++itr)
            *itr = glglT::move(*ptr);
        mid = itr;
        for (T *pe = buf + buf_len; pe-- != buf_end; ++itr)
            *itr = glglT::move(*pe);
        return mid;
    }
    mid = first;
    advance(mid, len / 2);
    first = glglT::stable_partition(first, mid, len / 2, buf, buf_len, p);
    last = glglT::stable_partition(mid, last, len - len / 2, buf, buf_len, p);
    return glglT::rotate(first, mid, last);
}

template <typename It, typename Int, typename Pred> inline
It stable_partition(It first, It last, Int len, Pred p)
{
    if (len == 1)
        return p(*first) ? last : first;
    typedef typename remove_pointer<It>::type T;
    auto pa = glglT::get_temporary_buffer<T>(len);
    const auto r = glglT::stable_partition(first, last, len,
                                           pa.first, pa.second, p);
    glglT::return_temporary_buffer(pa.first);
    return r;
}

template <typename It, typename Pred> inline
It stable_partition(It first, It last, Pred p)
{
    if (first == last)
        return first;
    return glglT::stable_partition(first, last, glglT::distance(first, last), p);
}

template <typename It, typename Pred>
It partition_point(It first, It last, Pred p)
{
    for (last = glglT::real_last(first, last); first != last;) {
        It mid = first;
        const auto len = distance(first, last);
        advance(mid, len / 2);
        if (p(*mid)) {
            first = ++mid;
        } else
            last = mid;
    }
    return first;
}

template <typename Int> constexpr
Int heap_left(Int i) noexcept { return i * 2 + 1; }

template <typename Int> constexpr
Int heap_right(Int i) noexcept { return i * 2 + 2; }

template <typename It>
It is_heap_until(It first, It last)
{
    const auto len = last - first;
    if (len < 2)
        return last;
    const auto incnt = len / 2 - 1;
    for (typename remove_cv<decltype(len)>::type i = 0; i != incnt; ++i) {
        if (first[i] < first[glglT::heap_left(i)])
            return glglT::heap_left(i) + first;
        if (first[i] < first[glglT::heap_right(i)])
            return glglT::heap_right(i) + first;
    }
    if (first[incnt] < first[glglT::heap_left(incnt)])
        return glglT::heap_left(incnt) + first;
    if (len % 2 && first[incnt] < first[glglT::heap_right(incnt)])
        return glglT::heap_right(incnt) + first;
    return last;
}

template <typename It, typename Comp>
It is_heap_until(It first, It last, Comp c)
{
    const auto len = last - first;
    if (len < 2)
        return last;
    const auto incnt = len / 2 - 1;
    for (typename remove_cv<decltype(len)>::type i = 0; i != incnt; ++i) {
        if (c(first[i], first[glglT::heap_left(i)]))
            return glglT::heap_left(i) + first;
        if (c(first[i], first[glglT::heap_right(i)]))
            return glglT::heap_right(i) + first;
    }
    if (c(first[incnt], first[glglT::heap_left(incnt)]))
        return glglT::heap_left(incnt) + first;
    if (len % 2 && c(first[incnt], first[glglT::heap_right(incnt)]))
        return glglT::heap_right(incnt) + first;
    return last;
}

template <typename It> inline
bool is_heap(It first, It last)
{
    return glglT::is_heap_until(first, last) == last;
}

template <typename It, typename Comp> inline
bool is_heap(It first, It last, Comp c)
{
    return glglT::is_heap_until(first, last, c) == last;
}

template <typename It, typename Int>
void heap_fix(It first, Int i, Int incnt, bool is_odd)
{
    while (true) {
        if (first[i] < first[glglT::heap_right(i)] &&
            first[glglT::heap_left(i)] < first[glglT::heap_right(i)]) {
            glglT::iter_swap(first + i, first + glglT::heap_right(i));
            i = glglT::heap_right(i);
        } else if (first[i] < first[glglT::heap_left(i)]) {
            glglT::iter_swap(first + i, first + glglT::heap_left(i));
            i = glglT::heap_left(i);
        } else
            return;
        if (i == incnt) {
            if (first[i] < first[heap_left(i)])
                glglT::iter_swap(first + i, first + glglT::heap_left(i));
            if (is_odd && first[i] < first[glglT::heap_right(i)])
                glglT::iter_swap(first + i, first + glglT::heap_right(i));
            return;
        } else if (incnt < i)
            return;
    }
}

template <typename It, typename Int, typename Comp>
void heap_fix(It first, Int i, Int incnt, bool is_odd, Comp c)
{
    while (true) {
        if (c(first[i], first[glglT::heap_right(i)]) &&
            c(first[glglT::heap_left(i)], first[glglT::heap_right(i)])) {
            glglT::iter_swap(first + i, first + glglT::heap_right(i));
            i = glglT::heap_right(i);
        } else if (c(first[i], first[glglT::heap_left(i)])) {
            glglT::iter_swap(first + i, first + glglT::heap_left(i));
            i = glglT::heap_left(i);
        } else
            return;
        if (i == incnt) {
            if (c(first[i], first[glglT::heap_left(i)]))
                glglT::iter_swap(first + i, first + glglT::heap_left(i));
            if (is_odd && c(first[i], first[glglT::heap_right(i)]))
                glglT::iter_swap(first + i, first + glglT::heap_right(i));
            return;
        } else if (incnt < i)
            return;
    }
}

template <typename It>
void make_heap(It first, It last)
{
    const auto len = last - first;
    if (len < 2)
        return;
    const auto incnt = len / 2 - 1;
    const bool is_odd = len % 2;
    auto i = incnt;
    if (first[i] < first[glglT::heap_left(i)])
        glglT::iter_swap(first + i, first + glglT::heap_left(i));
    if (is_odd && first[i] < first[glglT::heap_right(i)])
        glglT::iter_swap(first + i, first + glglT::heap_right(i));
    while (i-- != 0)
        glglT::heap_fix(first, i, incnt, is_odd);
}

template <typename It, typename Comp>
void make_heap(It first, It last, Comp c)
{
    const auto len = last - first;
    if (len < 2)
        return;
    const auto incnt = len / 2 - 1;
    const bool is_odd = len % 2;
    auto i = incnt;
    if (c(first[i], first[glglT::heap_left(i)]))
        glglT::iter_swap(first + i, first + glglT::heap_left(i));
    if (is_odd && c(first[i], first[glglT::heap_right(i)]))
        glglT::iter_swap(first + i, first + glglT::heap_right(i));
    while (i-- != 0)
        glglT::heap_fix(first, i, incnt, is_odd, c);
}

template <typename Int> constexpr
Int heap_root(Int i) noexcept { return (i - 1) / 2; }

template <typename It>
void push_heap(It first, It last)
{
    auto len = last - first;
    if (len < 2)
        return;
    --len;
    auto root = glglT::heap_root(len);
    while (true) {
        if (!(first[root] < first[len]))
            return;
        glglT::iter_swap(first + root, first + len);
        if (root == 0)
            return;
        len = root;
        root = glglT::heap_root(len);
    }
}

template <typename It, typename Comp>
void push_heap(It first, It last, Comp c)
{
    auto len = last - first;
    if (len < 2)
        return;
    --len;
    auto root = glglT::heap_root(len);
    while (true) {
        if (!c(first[root], first[len]))
            return;
        glglT::iter_swap(first + root, first + len);
        if (root == 0)
            return;
        len = root;
        root = glglT::heap_root(len);
    }
}

template <typename It> inline
void pop_heap(It first, It last)
{
    const auto len = last - first;
    if (len < 2)
        return;
    glglT::iter_swap(first, last - 1);
    if (len == 3) {
        if (*first < *(first + 1))
            glglT::iter_swap(first, first + 1);
    } else if (len != 2)
        glglT::heap_fix(first, decltype(len)(0), (len - 1) / 2 - 1, len % 2 == 0);
}

template <typename It, typename Comp> inline
void pop_heap(It first, It last, Comp c)
{
    const auto len = last - first;
    if (len < 2)
        return;
    glglT::iter_swap(first, last - 1);
    if (len == 3) {
        if (c(*first, *(first + 1)))
            glglT::iter_swap(first, first + 1);
    } else if (len != 2)
        glglT::heap_fix(first, decltype(len)(0), (len - 1) / 2 - 1, len % 2 == 0, c);
}

template <typename It>
void sort_heap(It first, It last)
{
    while (first != last)
        glglT::pop_heap(first, last), --last;
}

template <typename It, typename Comp>
void sort_heap(It first, It last, Comp c)
{
    while (first != last)
        glglT::pop_heap(first, last, c), --last;
}

template <typename It>
void partial_sort(It first, It middle, It last)
{
    glglT::make_heap(first, middle);
    const auto len = middle - first;
    const auto incnt = len / 2 - 1;
    const bool is_odd = len % 2;
    last = glglT::real_last(glglT::real_last(first, middle), last);
    for (It i = middle; i != last; ++i) {
        if (*i < *first) {
            glglT::iter_swap(first, i);
            glglT::heap_fix(first, decltype(len)(0), incnt, is_odd);
        }
    }
    glglT::sort_heap(first, middle);
}

template <typename It, typename Comp>
void partial_sort(It first, It middle, It last, Comp c)
{
    glglT::make_heap(first, middle, c);
    const auto len = middle - first;
    const auto incnt = len / 2 - 1;
    const bool is_odd = len % 2;
    last = glglT::real_last(glglT::real_last(first, middle), last);
    for (It i = middle; i != last; ++i) {
        if (c(*i, *first)) {
            glglT::iter_swap(first, i);
            glglT::heap_fix(first, decltype(len)(0), incnt, is_odd, c);
        }
    }
    glglT::sort_heap(first, middle, c);
}

template <typename It>
It is_sorted_until(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return last;
    It next = first;
    ++next;
    while (next != last && !(*next < *first))
        ++first, ++next;
    return next;
}

template <typename It, typename Comp>
It is_sorted_until(It first, It last, Comp c)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return last;
    It next = first;
    ++next;
    while (next != last && !c(*next, *first))
        ++first, ++next;
    return next;
}

template <typename It> inline
bool is_sorted(It first, It last)
{
    return glglT::is_sorted_until(first, last) == last;
}

template <typename It, typename Comp> inline
bool is_sorted(It first, It last, Comp c)
{
    return glglT::is_sorted_until(first, last, c) == last;
}

template <typename It>
void insertion_sort(It first, It last)
{
    for (It beg = first, bef = first; ++first != last; bef = first) {
        for (It next = first; *next < *bef; --next, --bef) {
            glglT::iter_swap(next, bef);
            if (bef == beg)
                break;
        }
    }
}

template <typename It, typename Comp>
void insertion_sort(It first, It last, Comp c)
{
    for (It beg = first, bef = first; ++first != last; bef = first) {
        for (It next = first; c(next, bef); --next, --bef) {
            glglT::iter_swap(next, bef);
            if (bef == beg)
                break;
        }
    }
}

template <typename T>
struct sort_lim_t: integral_constant<unsigned char, 16> {};

template <typename It>
using iter_sort_lim_t = sort_lim_t<typename remove_cv<
    typename remove_reference<decltype(*declval<It>())>::type
>::type>;

template <typename It, typename Int>
void three_part_intro_sort(It first, It last, Int len, Int deep)
{
    while (iter_sort_lim_t<It>::value < len) {
        if (deep-- == 0) {
            glglT::make_heap(first, last);
            glglT::sort_heap(first, last);
            return;
        }
        It mid = first, tmid = last;
        if (*--tmid < *mid)
            glglT::iter_swap(mid, tmid);
        for (It head = mid, tail = tmid, curr = ++mid; curr < tmid;) {
            if (!(*head < *curr)) {
                glglT::iter_swap(mid++, curr++);
            } else if (!(*curr < *tail)) {
                glglT::iter_swap(--tmid, curr);
                continue;
            } else
                ++curr;
        }
        glglT::three_part_intro_sort(first, mid, mid - first, deep);
        glglT::three_part_intro_sort(mid, tmid, tmid - mid, deep);
        first = tmid;
        len = last - first;
    }
    if (len > 0)
        glglT::insertion_sort(first, last);
}

template <typename It, typename Int, typename Comp>
void three_part_intro_sort(It first, It last, Int len, Int deep, Comp c)
{
    while (iter_sort_lim_t<It>::value < len) {
        if (deep-- == 0) {
            glglT::make_heap(first, last);
            glglT::sort_heap(first, last);
            return;
        }
        It mid = first, tmid = last;
        if (c(*--tmid, *mid))
            glglT::iter_swap(mid, tmid);
        for (It head = mid, tail = tmid, curr = ++mid; curr < tmid;) {
            if (!c(*head, *curr)) {
                glglT::iter_swap(mid++, curr++);
            } else if (!c(*curr, *tail)) {
                glglT::iter_swap(--tmid, curr);
                continue;
            } else
                ++curr;
        }
        glglT::three_part_intro_sort(first, mid, mid - first, deep);
        glglT::three_part_intro_sort(mid, tmid, tmid - mid, deep);
        first = tmid;
        len = last - first;
    }
    if (len > 0)
        glglT::insertion_sort(first, last, c);
}

template <typename It> inline
void sort(It first, It last)
{
    const auto len = last - first;
    three_part_intro_sort(first, last, len, decltype(len)(log2(len)) * 2);
}

template <typename It, typename Comp> inline
void sort(It first, It last, Comp c)
{
    const auto len = last - first;
    three_part_intro_sort(first, last, len, decltype(len)(log2(len)) * 2, c);
}

template <typename It1, typename It2>
It2 partial_sort_copy(It1 first1, It1 last1, It2 first2, It2 last2)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    It1 it1 = first1;
    for (It2 it2 = first2; it2 != last2; *it2 = *it1, ++it1, ++it2) {
        if (it1 == last1) {
            glglT::sort(first2, it2);
            return it2;
        }
    }
    glglT::make_heap(first2, last2);
    const auto len = last2 - first2;
    const auto incnt = len / 2 - 1;
    const auto is_odd = len % 2;
    while (it1 != last1) {
        if (*it1 < *first2) {
            *first2 = *it1;
            glglT::heap_fix(first2, decltype(len)(0), incnt, is_odd);
        }   ++it1;
    }
    glglT::sort_heap(first2, last2);
    return last2;
}

template <typename It1, typename It2, typename Comp>
It2 partial_sort_copy(It1 first1, It1 last1, It2 first2, It2 last2, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    It1 it1 = first1;
    for (It2 it2 = first2; it2 != last2; *it2 = *it1, ++it1, ++it2) {
        if (it1 == last1) {
            glglT::sort(first2, it2, c);
            return it2;
        }
    }
    glglT::make_heap(first2, last2, c);
    const auto len = last2 - first2;
    const auto incnt = len / 2 - 1;
    const auto is_odd = len % 2;
    while (it1 != last1) {
        if (c(*it1, *first2)) {
            *first2 = *it1;
            glglT::heap_fix(first2, decltype(len)(0), incnt, is_odd, c);
        }   ++it1;
    }
    glglT::sort_heap(first2, last2, c);
    return last2;
}

template <typename It>
void nth_element(It first, It nth, It last)
{
    for (auto len = last - first; len < 1;) {
        It middle = first + len / 2;
        middle = glglT::partition(first, last, [middle](decltype(*middle) r){
                                    return r < *middle;
                                  });
        if (middle == nth) {
            return;
        } else if (middle < nth) {
            first = middle;
            len = len - len / 2;
        } else {
            last = middle;
            len /= 2;
        }
    }
}

template <typename It, typename Comp>
void nth_element(It first, It nth, It last, Comp c)
{
    for (auto len = last - first; len < 1;) {
        It middle = first + len / 2;
        middle = glglT::partition(first, last, [middle, c](decltype(*middle) r){
                                    return c(r, *middle);
                                  });
        if (middle == nth) {
            return;
        } else if (middle < nth) {
            first = middle;
            len = len - len / 2;
        } else {
            last = middle;
            len /= 2;
        }
    }
}

template <typename It1, typename It2, typename It>
It merge(It1 first1, It1 last1, It2 first2, It2 last2, It first)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    while (true) {
        if (first1 == last1) {
            while (first2 != last2) {
                *first = *first2;
                ++first2;
                ++first;
            }   break;
        }
        if (first2 == last2) {
            do {
                *first = *first1;
                ++first;
                ++first1;
            } while (first1 != last1);
            break;
        }
        if (!(*first2 < *first1)) {
            *first = *first1;
            ++first1;
        } else {
            *first = *first2;
            ++first2;
        }   ++first;
    }
    return first;
}

template <typename It1, typename It2, typename It, typename Comp>
It merge(It1 first1, It1 last1, It2 first2, It2 last2, It first, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    while (true) {
        if (first1 == last1) {
            while (first2 != last2) {
                *first = *first2;
                ++first2;
                ++first;
            }   break;
        }
        if (first2 == last2) {
            do {
                *first = *first1;
                ++first;
                ++first1;
            } while (first1 != last1);
            break;
        }
        if (!c(*first2, *first1)) {
            *first = *first1;
            ++first1;
        } else {
            *first = *first2;
            ++first2;
        }   ++first;
    }
    return first;
}

template <typename It, typename Ptr, typename Int>
void inplace_merge(It first, It middle, It last, Int len, Ptr buf, Int buf_len)
{
    if (len <= buf_len) {
        glglT::merge(first, middle, middle, last, buf);
        glglT::move(buf, buf + len, first);
        return;
    }
    auto half = len / 2;
    It m1 = first, m2 = middle;
    while (half-- != 0) {
        if (*m1 < *m2) {
            ++m1;
            if (m1 == middle)
                break;
        } else {
            ++m2;
            if (m2 == last)
                break;
        }
    }
    middle = glglT::rotate(m1, middle, m2);
    if (first != m1 && m1 != middle)
        glglT::inplace_merge(first, m1, middle,
                             glglT::distance(first, middle), buf, buf_len);
    if (middle != m2 && m2 != last)
        glglT::inplace_merge(middle, m2, last,
                             glglT::distance(middle, last), buf, buf_len);
}

template <typename It> inline
void inplace_merge(It first, It middle, It last)
{
    if (first == middle || middle == last)
        return;
    const auto len = glglT::distance(first, last);
    typedef typename remove_pointer<It>::type T;
    const auto p = glglT::get_temporary_buffer<T>(len);
    glglT::inplace_merge(first, middle, last, len, p.first, p.second);
    glglT::return_temporary_buffer(p.first);
}

template <typename It, typename Ptr, typename Int, typename Comp>
void inplace_merge(It first, It middle, It last,
                   Int len, Ptr buf, Int buf_len, Comp c)
{
    if (len <= buf_len) {
        glglT::merge(first, middle, middle, last, buf, c);
        return;
    }
    auto half = len / 2;
    It m1 = first, m2 = middle;
    while (half-- != 0) {
        if (c(*m1, *m2)) {
            ++m1;
            if (m1 == middle)
                break;
        } else {
            ++m2;
            if (m2 == last)
                break;
        }
    }
    middle = glglT::rotate(m1, middle, m2);
    if (first != m1 && m1 != middle)
        glglT::inplace_merge(first, m1, middle,
                             glglT::distance(first, middle), buf, buf_len, c);
    if (middle != m2 && m2 != last)
        glglT::inplace_merge(middle, m2, last,
                             glglT::distance(middle, last), buf, buf_len, c);
}

template <typename It, typename Comp> inline
void inplace_merge(It first, It middle, It last, Comp c)
{
    if (first == middle || middle == last)
        return;
    const auto len = glglT::distance(first, last);
    typedef typename remove_pointer<It>::type T;
    const auto p = glglT::get_temporary_buffer<T>(len);
    glglT::inplace_merge(first, middle, last, len, p.first, p.second, c);
    glglT::return_temporary_buffer(p.first);
}

template <typename It, typename Int>
void inplace_merge_sort(It first, It last, Int len)
{
    if (iter_sort_lim_t<It>::value < len) {
        const auto middle = first + len / 2;
        glglT::inplace_merge_sort(first, middle, len / 2);
        glglT::inplace_merge_sort(middle, last, len - len / 2);
        glglT::inplace_merge(first, middle, last, len, It (nullptr), decltype(len)(0));
        return;
    }
    if (len > 0)
        glglT::insertion_sort(first, last);
}

template <typename It, typename Int, typename Comp>
void inplace_merge_sort(It first, It last, Int len, Comp c)
{
    if (iter_sort_lim_t<It>::value < len) {
        const auto middle = first + len / 2;
        glglT::inplace_merge_sort(first, middle, len / 2, c);
        glglT::inplace_merge_sort(middle, last, len - len / 2, c);
        glglT::inplace_merge(first, middle, last, len, It (nullptr), decltype(len)(0), c);
        return;
    }
    if (len > 0)
        glglT::insertion_sort(first, last, c);
}

template <typename It, typename Int, typename Ptr>
void merge_sort(It first, It last, Int len, Ptr buf, Int buf_len)
{
    if (iter_sort_lim_t<It>::value < len) {
        const auto middle = first + len / 2;
        glglT::merge_sort(first, middle, len / 2, buf, buf_len);
        glglT::merge_sort(middle, last, len - len / 2, buf, buf_len);
        if (buf_len < len) {
            glglT::inplace_merge(first, middle, last, len, buf, buf_len);
        } else {
            glglT::merge(first, middle, middle, last, buf);
            glglT::move(buf, buf + len, first);
        }
        return;
    }
    if (len != 0)
        glglT::insertion_sort(first, last);
}

template <typename It, typename Int, typename Ptr, typename Comp>
void merge_sort(It first, It last, Int len, Ptr buf, Int buf_len, Comp c)
{
    if (iter_sort_lim_t<It>::value < len) {
        const auto middle = first + len / 2;
        glglT::merge_sort(first, middle, len / 2, buf, buf_len, c);
        glglT::merge_sort(middle, last, len - len / 2, buf, buf_len, c);
        if (buf_len < len) {
            glglT::inplace_merge(first, middle, last, len, buf, buf_len, c);
        } else {
            glglT::merge(first, middle, middle, last, buf, c);
            glglT::move(buf, buf + len, first);
        }
        return;
    }
    if (len != 0)
        glglT::insertion_sort(first, last, c);
}

template <typename It> inline
void stable_sort(It first, It last)
{
    if (first == last)
        return;
    const auto len = last - first;
    typedef typename remove_reference<decltype(*first)>::type T;
    const auto p = glglT::get_temporary_buffer<T>(len);
    if (p.first) {
        glglT::merge_sort(first, last, len, p.first, p.second);
        glglT::return_temporary_buffer(p.first);
    } else
        glglT::inplace_merge_sort(first, last, len);
}

template <typename It, typename Comp> inline
void stable_sort(It first, It last, Comp c)
{
    if (first == last)
        return;
    const auto len = last - first;
    typedef typename remove_reference<decltype(*first)>::type T;
    const auto p = glglT::get_temporary_buffer<T>(len);
    if (p.first) {
        glglT::merge_sort(first, last, len, p.first, p.second, c);
        glglT::return_temporary_buffer(p.first);
    } else
        glglT::inplace_merge_sort(first, last, len, c);
}

template <typename It, typename T>
It lower_bound(It first, It last, const T &t)
{
    auto len = glglT::distance(first, last);
    It middle;
    while (len > 0) {
        middle = first;
        glglT::advance(middle, len / 2);
        if (*middle < t) {
            first = middle;
            ++first;
            len = len - len / 2 - 1;
        } else {
            last = middle;
            len /= 2;
        }
    }
    return first;
}

template <typename It, typename T, typename Comp>
It lower_bound(It first, It last, const T &t, Comp c)
{
    auto len = glglT::distance(first, last);
    It middle;
    while (len > 0) {
        middle = first;
        glglT::advance(middle, len / 2);
        if (c(*middle, t)) {
            first = middle;
            ++first;
            len = len - len / 2 - 1;
        } else {
            last = middle;
            len /= 2;
        }
    }
    return first;
}

template <typename It, typename T>
It upper_bound(It first, It last, const T &t)
{
    auto len = glglT::distance(first, last);
    It middle;
    while (len > 0) {
        middle = first;
        glglT::advance(middle, len / 2);
        if (t < *middle) {
            last = middle;
            len /= 2;
        } else {
            first = middle;
            ++first;
            len = len - len / 2 - 1;
        }
    }
    return first;
}

template <typename It, typename T, typename Comp>
It upper_bound(It first, It last, const T &t, Comp c)
{
    auto len = glglT::distance(first, last);
    It middle;
    while (len > 0) {
        middle = first;
        glglT::advance(middle, len / 2);
        if (c(t, *middle)) {
            last = middle;
            len /= 2;
        } else {
            first = middle;
            ++first;
            len = len - len / 2 - 1;
        }
    }
    return first;
}

template <typename It, typename T> inline
bool binary_search(It first, It last, const T &t)
{
    last = glglT::real_last(first, last);
    const auto it = glglT::lower_bound(first, last, t);
    return it != last && !(t < *it);
}

template <typename It, typename T, typename Comp> inline
bool binary_search(It first, It last, const T &t, Comp c)
{
    last = glglT::real_last(first, last);
    const auto it = glglT::lower_bound(first, last, t, c);
    return it != last && !c(t, *it);
}

template <typename It, typename T> inline
pair<It, It> equal_range(It first, It last, const T &t)
{
    return glglT::make_pair(glglT::lower_bound(first, last, t),
                            glglT::upper_bound(first, last, t));
}

template <typename It, typename T, typename Comp> inline
pair<It, It> equal_range(It first, It last, const T &t, Comp c)
{
    return glglT::make_pair(glglT::lower_bound(first, last, t, c),
                            glglT::upper_bound(first, last, t, c));
}

template <typename It1, typename It2>
bool includes(It1 first1, It1 last1, It2 first2, It2 last2,
              random_access_iterator_tag)
{
    if (first2 == last2)
        return true;
    first1 = glglT::lower_bound(first1, last1, *first2);
    for (; first1 != last1 && !(*first2 < *first1); ++first1)
        if (!(*first1 < *first2))
            if (++first2 == last2)
                return true;
    return false;
}

template <typename It1, typename It2, typename Comp>
bool includes(It1 first1, It1 last1, It2 first2, It2 last2, Comp c,
              random_access_iterator_tag)
{
    if (first2 == last2)
        return true;
    first1 = glglT::lower_bound(first1, last1, *first2, c);
    for (; first1 != last1 && !c(*first2, *first1); ++first1)
        if (!c(*first1, *first2))
            if (++first2 == last2)
                return true;
    return false;
}

template <typename It1, typename It2>
bool includes(It1 first1, It1 last1, It2 first2, It2 last2, input_iterator_tag)
{
    if (first2 == last2)
        return true;
    for (; first1 != last1 && !(*first2 < *first1); ++first1)
        if (!(*first1 < *first2))
            if (++first2 == last2)
                return true;
    return false;
}

template <typename It1, typename It2, typename Comp>
bool includes(It1 first1, It1 last1, It2 first2, It2 last2, Comp c,
              input_iterator_tag)
{
    if (first2 == last2)
        return true;
    for (; first1 != last1 && !c(*first2, *first1); ++first1)
        if (!c(*first1, *first2))
            if (++first2 == last2)
                return true;
    return false;
}

template <typename It1, typename It2> inline
bool includes(It1 first1, It1 last1, It2 first2, It2 last2)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    return glglT::includes(first1, last1, first2, last2,
                           typename iterator_traits<It1>::iterator_category());
}

template <typename It1, typename It2, typename Comp> inline
bool includes(It1 first1, It1 last1, It2 first2, It2 last2, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    return glglT::includes(first1, last1, first2, last2, c,
                           typename iterator_traits<It1>::iterator_category());
}

template <typename It1, typename It2, typename It>
It set_difference(It1 first1, It1 last1, It2 first2, It2 last2, It first)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    while (first1 != last1) {
        while (true) {
            if (*first1 < *first2) {
                *first = *first1;
                ++first;
                ++first1;
                break;
            }
            const auto is_eq = (!(*first2 < *first1));
            if (is_eq)
                ++first1;
            if (++first2 == last2)
                return glglT::copy(first1, last1, first);
            if (is_eq)
                break;
        }
    }
    return first;
}

template <typename It1, typename It2, typename It, typename Comp>
It set_difference(It1 first1, It1 last1, It2 first2, It2 last2, It first, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    while (first1 != last1) {
        while (true) {
            if (c(*first1, *first2)) {
                *first = *first1;
                ++first;
                ++first1;
                break;
            }
            const auto is_eq = (!c(*first2, *first1));
            if (is_eq)
                ++first1;
            if (++first2 == last2)
                return glglT::copy(first1, last1, first);
            if (is_eq)
                break;
        }
    }
    return first;
}

template <typename It1, typename It2, typename It>
It set_intersection(It1 first1, It1 last1, It2 first2, It2 last2, It first)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return first;
    while (first1 != last1) {
        while (true) {
            if (*first1 < *first2) {
                ++first1;
                break;
            }
            const auto is_eq = (!(*first2 < *first1));
            if (is_eq) {
                *first = *first1;
                ++first1;
                ++first;
            }
            if (++first2 == last2)
                return first;
            if (is_eq)
                break;
        }
    }
    return first;
}

template <typename It1, typename It2, typename It, typename Comp>
It set_intersection(It1 first1, It1 last1, It2 first2, It2 last2, It first, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return first;
    while (first1 != last1) {
        while (true) {
            if (c(*first1, *first2)) {
                ++first1;
                break;
            }
            const auto is_eq = (!c(*first2, *first1));
            if (is_eq) {
                *first = *first1;
                ++first1;
                ++first;
            }
            if (++first2 == last2)
                return first;
            if (is_eq)
                break;
        }
    }
    return first;
}

template <typename It1, typename It2, typename It>
It set_symmetric_difference(It1 first1, It1 last1, It2 first2, It2 last2, It first)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    while (first1 != last1) {
        while (true) {
            if (*first1 < *first2) {
                *first = *first1;
                ++first;
                ++first1;
                break;
            }
            const auto is_eq = (!(*first2 < *first1));
            if (!is_eq) {
                ++first1;
            } else {
                *first = *first2;
                ++first;
            }
            if (++first2 == last2)
                return glglT::copy(first1, last1, first);
            if (is_eq)
                break;
        }
    }
    return glglT::copy(first2, last2, first);
}

template <typename It1, typename It2, typename It, typename Comp>
It set_symmetric_difference(It1 first1, It1 last1, It2 first2, It2 last2, It first, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    while (first1 != last1) {
        while (true) {
            if (c(*first1, *first2)) {
                *first = *first1;
                ++first;
                ++first1;
                break;
            }
            const auto is_eq = (!c(*first2, *first1));
            if (!is_eq) {
                ++first1;
            } else {
                *first = *first2;
                ++first;
            }
            if (++first2 == last2)
                return glglT::copy(first1, last1, first);
            if (is_eq)
                break;
        }
    }
    return glglT::copy(first2, last2, first);
}

template <typename It1, typename It2, typename It>
It set_union(It1 first1, It1 last1, It2 first2, It2 last2, It first)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    for (; first1 != last1; ++first) {
        while (true) {
            if (*first1 < *first2) {
                *first = *first1;
                ++first1;
                break;
            }
            *first = *first2;
            ++first2;
            const auto will_break = !(*first2 < *first1);
            if (will_break)
                ++first1;
            if (first2 == last2)
                return glglT::copy(first1, last1, first);
            if (will_break)
                break;
        }
    }
    return glglT::copy(first2, last2, first);
}

template <typename It1, typename It2, typename It, typename Comp>
It set_union(It1 first1, It1 last1, It2 first2, It2 last2, It first, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    if (first2 == last2)
        return glglT::copy(first1, last1, first);
    for (; first1 != last1; ++first) {
        while (true) {
            if (c(*first1, *first2)) {
                *first = *first1;
                ++first1;
                break;
            }
            *first = *first2;
            ++first2;
            const auto will_break = !c(*first2, *first1);
            if (will_break) ++first1;
            if (first2 == last2)
                return glglT::copy(first1, last1, first);
            if (will_break) break;
        }
    }
    return glglT::copy(first2, last2, first);
}

template <typename It>
It max_element(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    It res = first;
    for (++first; first != last; ++first)
        if (*res < *first)
            res = first;
    return res;
}

template <typename It, typename Comp>
It max_element(It first, It last, Comp)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    It res = first;
    for (++first; first != last; ++first)
        if (c(*res, *first))
            res = first;
    return res;
}

template <typename T> inline
const T &max(const T &t1, const T &t2)
{
    return t1 < t2 ? t2 : t1;
}

template <typename T, typename Comp> inline
const T &max(const T &t1, const T &t2, Comp c)
{
    return c(t1, t2) ? t2 : t1;
}

template <typename T> inline
T max(std::initializer_list<T> il)
{
    return *max_element(il.begin(), il.end());
}

template <typename T, typename Comp> inline
T max(std::initializer_list<T> il, Comp c)
{
    return *max_element(il.begin(), il.end(), c);
}

template <typename It>
It min_element(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    It res = first;
    for (++first; first != last; ++first)
        if (*first < *res)
            res = first;
    return res;
}

template <typename It, typename Comp>
It min_element(It first, It last, Comp)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    It res = first;
    for (++first; first != last; ++first)
        if (c(*first, *res))
            res = first;
    return res;
}

template <typename T> inline
const T &min(const T &t1, const T &t2)
{
    return t1 < t2 ? t1 : t2;
}

template <typename T, typename Comp> inline
const T &min(const T &t1, const T &t2, Comp c)
{
    return c(t1, t2) ? t1 : t2;
}

template <typename T> inline
T min(std::initializer_list<T> il)
{
    return *min_element(il.begin(), il.end());
}

template <typename T, typename Comp> inline
T min(std::initializer_list<T> il, Comp c)
{
    return *min_element(il.begin(), il.end(), c);
}

template <typename It>
pair<It, It> minmax_element(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    pair<It, It> res{first, first};
    for (++first; first != last; ++first) {
        if (*(res.second) < *first)
            res.second = first;
         else if (*first < *(res.first))
            res.first = first;
    }
    return res;
}

template <typename It, typename Comp>
pair<It, It> minmax_element(It first, It last, Comp)
{
    last = glglT::real_last(first, last);
    if (first != last)
        return first;
    pair<It, It> res{first, first};
    for (++first; first != last; ++first) {
        if (c(*(res.second), *first))
            res.second = first;
         else if (c(*first, *(res.first)))
            res.first = first;
    }
    return res;
}

template <typename T> inline
pair<const T &, const T &> minmax(const T &t1, const T &t2)
{
    return t1 < t2 ? pair<const T &, const T &>(t1, t2)
                   : pair<const T &, const T &>(t2, t1);
}

template <typename T, typename Comp> inline
const T &minmax(const T &t1, const T &t2, Comp c)
{
    return c(t1, t2) ? pair<const T &, const T &>(t1, t2)
                     : pair<const T &, const T &>(t2, t1);
}

template <typename T> inline
pair<T, T> minmax(std::initializer_list<T> il)
{
    const auto p = glglT::minmax_element(il.begin(), il.end());
    return glglT::make_pair(*(p.first), *(p.second));
}

template <typename T, typename Comp> inline
pair<T, T> minmax(std::initializer_list<T> il, Comp c)
{
    const auto p = glglT::minmax_element(il.begin(), il.end(), c);
    return glglT::make_pair(*(p.first), *(p.second));
}

template <typename It1, typename It2>
bool lexicographical_compare(It1 first1, It1 last1, It2 first2, It2 last2)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    while (true) {
        if (first2 == last2) return false;
        if (first1 == last1) return true;
        if (*first1 < *first2) return true;
        if (*first2 < *first1) return false;
        ++first1; ++first2;
    }
}

template <typename It1, typename It2, typename Comp>
bool lexicographical_compare(It1 first1, It1 last1, It2 first2, It2 last2, Comp c)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    while (true) {
        if (first2 == last2) return false;
        if (first1 == last1) return true;
        if (c(*first1, *first2)) return true;
        if (c(*first2, *first1)) return false;
        ++first1; ++first2;
    }
}

template <typename T,
          typename = typename enable_if<std::is_trivially_copyable<T>::value &&
                                        sizeof(T) == 1>::type> inline
bool lexicographical_compare(T *first1, T *last1, T *first2, T *last2)
{
    last1 = glglT::real_last(first1, last1);
    last2 = glglT::real_last(first2, last2);
    const auto sz1 = last1 - first1, sz2 = last2 - first2;
    const auto sz = sz1 < sz2 ? sz1 : sz2;
    const auto i = std::memcmp(first1, first2, sz);
    return i == 0 ? (sz1 < sz2 ? true : false) : i < 0;
}

template <typename It1, typename It2>
bool is_permutation(It1 first1, It1 last, It2 first2)
{
    last = glglT::real_last(first1, last);
    const auto p = glglT::mismatch(first1, last, first2);
    first1 = p.first;
    first2 = p.second;
    if (first1 == last)
        return true;
    It2 last2 = first2;
    auto len = glglT::distance(first1, last);
    glglT::advance(last2, len);
    do {
        const auto it = glglT::find(first1, last, *first2);
        if (it == last)
            return false;
        len -= glglT::count(it, last, *first2);
        if (++first2 == last2)
            return true;
        if (len == 0)
            return false;
    } while (true);
}

template <typename It1, typename It2, typename Pred>
bool is_permutation(It1 first1, It1 last, It2 first2, Pred p)
{
    last = glglT::real_last(first1, last);
    const auto pa = glglT::mismatch(first1, last, first2, p);
    first1 = pa.first;
    first2 = pa.second;
    if (first1 == last)
        return true;
    It2 last2 = first2;
    auto len = glglT::distance(first1, last);
    glglT::advance(last2, len);
    do {
        const auto it = glglT::find(first1, last, *first2, p);
        if (it == last)
            return false;
        auto pc = [p, first2](decltype(*it) r) { return p(r, *first2); };
        len -= glglT::count_if(it, last, pc);
        if (++first2 == last2)
            return true;
        if (len == 0)
            return false;
    } while (true);
}

template <typename It>
bool next_permutation(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return false;
    It lbeg = last;
    if (--lbeg == first)
        return false;
    It first_smaller = lbeg;
    while (true) {
        --first_smaller;
        if (*first_smaller < *lbeg)
            break;
        if (first == first_smaller) {
            glglT::reverse(first, last);
            return false;
        }
        --lbeg;
    }
    do {
        ++lbeg;
    } while (lbeg != last && *first_smaller < *lbeg);
    glglT::iter_swap(first_smaller, --lbeg);
    glglT::sort(++first_smaller, last);
    return true;
}

template <typename It, typename Comp>
bool next_permutation(It first, It last, Comp c)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return false;
    It lbeg = last;
    if (--lbeg == first)
        return false;
    It first_smaller = lbeg;
    while (true) {
        --first_smaller;
        if (c(*first_smaller, *lbeg))
            break;
        if (first == first_smaller) {
            glglT::reverse(first, last);
            return false;
        }
        --lbeg;
    }
    do {
        ++lbeg;
    } while (lbeg != last && c(*first_smaller, *lbeg));
    glglT::iter_swap(first_smaller, --lbeg);
    glglT::reverse(++first_smaller, last);
    return true;
}

template <typename It>
bool prev_permutation(It first, It last)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return false;
    It lbeg = last;
    if (--lbeg == first)
        return false;
    It first_bigger = lbeg;
    while (true) {
        --first_bigger;
        if (*lbeg < *first_bigger)
            break;
        if (first == first_bigger) {
            glglT::reverse(first, last);
            return false;
        }
        --lbeg;
    }
    do {
        ++lbeg;
    } while (lbeg != last && *lbeg < *first_bigger);
    glglT::iter_swap(first_bigger, --lbeg);
    glglT::reverse(++first_bigger, last);
    return true;
}

template <typename It, typename Comp>
bool prev_permutation(It first, It last, Comp c)
{
    last = glglT::real_last(first, last);
    if (first == last)
        return false;
    It lbeg = last;
    if (--lbeg == first)
        return false;
    It first_bigger = lbeg;
    while (true) {
        --first_bigger;
        if (c(*lbeg, *first_bigger))
            break;
        if (first == first_bigger) {
            glglT::reverse(first, last);
            return false;
        }
        --lbeg;
    }
    do {
        ++lbeg;
    } while (lbeg != last && c(*lbeg, *first_bigger));
    glglT::iter_swap(first_bigger, --lbeg);
    glglT::reverse(++first_bigger, last);
    return true;
}

} // namespace glglT

#endif // GLGL_SORT_ALGO_H

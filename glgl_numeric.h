#ifndef GLGL_NUMERIC_H
#define GLGL_NUMERIC_H

#include "sub/glgl_memory_tool.h"

namespace glglT {

template <typename It, typename T>
void iota(It first, It last, T t)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first, ++t)
        *first = t;
}

template <typename It, typename T>
T accumulate(It first, It last, T init)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        init += *first;
    return init;
}

template <typename It, typename T, typename Oprt>
T accumulate(It first, It last, T init, Oprt op)
{
    last = glglT::real_last(first, last);
    for (; first != last; ++first)
        init = op(init, *first);
    return init;
}

template <typename It1, typename It2, typename T>
T inner_product(It1 first1, It1 last, It2 first2, T init)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
         init += *first1 * *first2;
    return init;
}

template <typename It1, typename It2, typename T, typename Op1, typename Op2>
T inner_product(It1 first1, It1 last, It2 first2, T init, Op1 op1, Op2 op2)
{
    last = glglT::real_last(first1, last);
    for (; first1 != last; ++first1, ++first2)
         init = op1(init,  op2(*first1, *first2));
    return init;
}

template <typename It1, typename It2>
It2 adjacent_difference(It1 first1, It1 last, It2 first2)
{
    typedef typename iterator_traits<It1>::value_type value_t;
    last = glglT::real_last(first1, last);
    for (value_t v = value_t(); first1 != last; ++first1, ++first2) {
        v = *first1 - v;
        *first2 = v;
    }
    return first2;
}

template <typename It1, typename It2, typename Oprt>
It2 adjacent_difference(It1 first1, It1 last, It2 first2, Oprt op)
{
    typedef typename iterator_traits<It1>::value_type value_t;
    last = glglT::real_last(first1, last);
    for (value_t v = value_t(); first1 != last; ++first1, ++first2) {
        v = op(*first1, v);
        *first2 = v;
    }
    return first2;
}

template <typename It1, typename It2>
It2 partial_sum(It1 first1, It1 last, It2 first2)
{
    typedef typename iterator_traits<It1>::value_type value_t;
    last = glglT::real_last(first1, last);
    for (value_t v = value_t(); first1 != last; ++first1, ++first2) {
        v += *first1;
        *first2 = v;
    }
    return first2;
}

template <typename It1, typename It2, typename Oprt>
It2 partial_sum(It1 first1, It1 last, It2 first2, Oprt op)
{
    typedef typename iterator_traits<It1>::value_type value_t;
    last = glglT::real_last(first1, last);
    for (value_t v = value_t(); first1 != last; ++first1, ++first2) {
        v = op(v, *first1);
        *first2 = v;
    }
    return first2;
}

} // namespace glglT

#endif // GLGL_NUMERIC_H

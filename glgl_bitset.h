#ifndef GLGL_BITSET_H
#define GLGL_BITSET_H

#include <climits>
#include <cstdint>
#include "glgl_string.h"

namespace glglT {

template <size_t> class bitset;

template <size_t N, bool = N % (sizeof(N) * CHAR_BIT)>
struct bitset_use_fields {
    size_t data[N / (sizeof(N) * CHAR_BIT)];
};

template <size_t N>
struct bitset_use_fields<N, true> {
    size_t data[N / (sizeof(N) * CHAR_BIT) + 1];
};

template <bool b>
struct bitset_use_fields<0, b> {
    size_t data[1];
};

template <size_t N, bool = N % (sizeof(N) * CHAR_BIT)>
struct bitset_all_use_judge {
    static constexpr bool all(const bitset_use_fields<N> &) noexcept
        { return true; }
};

template <size_t N>
struct bitset_all_use_judge<N, true> {
    static constexpr bool all(const bitset_use_fields<N> &data) noexcept
    {
        return data.data[N / (sizeof(N) * CHAR_BIT)] ==
        (std::numeric_limits<size_t>::max() >>
         (sizeof(N) * CHAR_BIT - (N % (sizeof(N) * CHAR_BIT))));
    }
};

template <size_t N, bool = N % (sizeof(N) * CHAR_BIT)>
struct bitset_set_use_set {
    static constexpr bitset<N> &set(bitset<N> &bs, bitset_use_fields<N> &)
        noexcept
    {
        return bs;
    }
};

template <size_t N>
struct bitset_set_use_set<N, true> {
    static bitset<N> &set(bitset<N> &bs, bitset_use_fields<N> &data) noexcept
    {
        data.data[N / (sizeof(N) * CHAR_BIT)] =
        (std::numeric_limits<size_t>::max() >>
         (sizeof(N) * CHAR_BIT - (N % (sizeof(N) * CHAR_BIT))));
        return bs;
    }
};

template <size_t N, bool = N % (sizeof(N) * CHAR_BIT)>
struct bitset_flip_use_flip {
    static constexpr bitset<N> &set(bitset<N> &bs, bitset_use_fields<N> &)
        noexcept
    {
        return bs;
    }
};

template <size_t N>
struct bitset_flip_use_flip<N, true> {
    static bitset<N> &set(bitset<N> &bs, bitset_use_fields<N> &data) noexcept
    {
        data.data[N / (sizeof(N) * CHAR_BIT)] ^=
        (std::numeric_limits<size_t>::max() >>
         (sizeof(N) * CHAR_BIT - (N % (sizeof(N) * CHAR_BIT))));
        return bs;
    }
};

template <size_t N>
class bitset {
    bitset_use_fields<N> data;

    friend class hash<bitset>;
    constexpr static size_t width = sizeof(size_t) * CHAR_BIT;
    constexpr static size_t length = sizeof(data.data) / sizeof(size_t);
public:
    class reference {
        friend class bitset;

        bitset_use_fields<N> *data;
        size_t n;
        size_t i;
    public:
        ~reference() {}

        reference &operator=(bool x) noexcept
        {
            if (x) data->data[n] |= size_t(1) << i;
            else data->data[n] &= ~(size_t(1) << i);
            return *this;
        }

        reference &operator=(const reference &r) noexcept
        {
            return this->operator=(bool(r));
        }

        constexpr operator bool () const noexcept
        {
            return (data->data[n] >> i) % 2;
        }

        constexpr bool operator~() const noexcept { return ~(bool(*this)); }

        reference &flip() noexcept
        {
            data->data[n] ^= size_t(1) << i;
            return *this;
        }
    private:
        constexpr reference(const bitset_use_fields<N> &d,
                            size_t sz1, size_t sz2) noexcept:
            data(const_cast<bitset_use_fields<N> *>(&d)), n(sz1), i(sz2) {}
    };

    constexpr bitset() noexcept: data{} {}
    constexpr bitset(unsigned long long val) noexcept:
        data({ val
#if SIZE_MAX == ULLONG_MAX
             }) {}
#else
             , val >> (sizeof(size_t) * CHAR_BIT) }) {}
#endif // SIZE_MAX

    template <typename C, typename T, typename A>
    explicit bitset(const basic_string<C, T, A> &s,
                    typename basic_string<C, T, A>::size_type pos = 0,
                    typename basic_string<C, T, A>::size_type n =
                        basic_string<C, T, A>::npos,
                    C zero = C('0'), C one = C('1')): data{}
    {
        const auto sz = s.size();
        if (sz < pos)
            throw std::out_of_range("basic_tring out of range");
        if (sz - pos < n)
            n = sz - pos;
        n += pos;
        const auto b = s.begin();
        for (size_t i = 0; n-- != pos; ++i) {
            const auto &c = b[n];
            if (c == one) {
                reference(data, i / width, i % width).flip();
            } else if (c != zero)
                throw std::invalid_argument("string is not a bitset");
        }
    }

    template <typename C>
    explicit bitset(const C *s, typename basic_string<C>::size_type n =
                        basic_string<C>::npos,
                    C zero = C('0'), C one = C('1')): data{}
    {
        if (n == basic_string<C>::npos)
            n = std::char_traits<C>::length(s);
        for (size_t i = 0; n-- != 0; ++i) {
            const auto &c = s[n];
            if (c == one) {
                reference(data, i / width, i % width).flip();
            } else if (c != zero)
                throw std::invalid_argument("string is not a bitset");
        }
    }

    bool operator==(const bitset &b) const noexcept
    {
        for (size_t n = 0; n != length; ++n)
            if (data.data[n] != b.data.data[n])
                return false;
        return true;
    }

    bool operator!=(const bitset &b) const noexcept { return !(*this == b); }
    constexpr size_t size() const noexcept { return N; }
    bitset &reset(size_t pos) { return this->set(pos, false); }
    bitset operator~() const noexcept { return bitset(*this).flip(); }

    constexpr bool operator[](size_t pos) const noexcept
    {
        return bool(reference(data, pos / width, pos % width));
    }

    reference operator[](size_t pos) noexcept
    {
        return reference(data, pos / width, pos % width);
    }

    bool test(size_t pos) const
    {
        if (!(pos < N))
            throw std::out_of_range("bitset out of range");
        return bool(reference(data, pos / width, pos % width));
    }

    bool all() const noexcept
    {
        for (size_t n = 0; n != (N / width); ++n)
            if (data.data[n] != std::numeric_limits<size_t>::max())
                return false;
        return bitset_all_use_judge<N>::all(data);
    }

    bool any() const noexcept
    {
        for (size_t n = 0; n != length; ++n)
            if (data.data[n] != 0)
                return true;
        return false;
    }

    bool none() const noexcept
    {
        for (size_t n = 0; n != length; ++n)
            if (data.data[n] != 0)
                return false;
        return true;
    }

    size_t count() const noexcept
    {
        size_t cnt = 0;
        for (size_t n = 0; n != N; ++n)
            cnt += bool(reference(data, n / width, n % width));
        return cnt;
    }

    bitset &set() noexcept
    {
        for (size_t n = 0; n != (N / width); ++n)
            data.data[n] = std::numeric_limits<size_t>::max();
        return bitset_set_use_set<N>::set(*this, data);
    }

    bitset &set(size_t pos, bool val = true)
    {
        if (!(pos < N))
            throw std::out_of_range("bitset out of range");
        reference(data, pos / width, pos % width).operator=(val);
        return *this;
    }

    bitset &reset() noexcept
    {
        for (size_t n = 0; n != length; ++n)
            data.data[n] = 0;
        return *this;
    }

    bitset &flip() noexcept
    {
        for (size_t n = 0; n != (N / width); ++n)
            data.data[n] = ~data.data[n];
        return bitset_flip_use_flip<N>::set(*this, data);
    }

    bitset &flip(size_t pos)
    {
        if (!(pos < N))
            throw std::out_of_range("bitset out of range");
        reference(data, pos / width, pos % width).flip();
        return *this;
    }

    bitset &operator&=(const bitset &bs) noexcept
    {
        for (size_t n = 0; n != length; ++n)
            data.data[n] &= bs.data.data[n];
        return *this;
    }

    bitset &operator|=(const bitset &bs) noexcept
    {
        for (size_t n = 0; n != length; ++n)
            data.data[n] |= bs.data.data[n];
        return *this;
    }

    bitset &operator^=(const bitset &bs) noexcept
    {
        for (size_t n = 0; n != length; ++n)
            data.data[n] ^= bs.data.data[n];
        return *this;
    }

    bitset &operator>>=(size_t pos) noexcept
    {
        const auto tail = pos % width;
        const auto cnt = pos / width;
        size_t n = 0;
        for (size_t i = cnt; i != length; ++n, ++i)
            data.data[n] = data.data[i];
        for (size_t i = n; i != length; ++i)
            data.data[i] = 0;
        if (tail)
            for (size_t down = 0, tmp; n-- != 0;) {
                tmp = data.data[n] & (std::numeric_limits<size_t>::max() >>
                                      (width - tail));
                tmp <<= width - tail;
                data.data[n] >>= tail;
                data.data[n] |= down;
                down = tmp;
            }
        return *this;
    }

    bitset &operator<<=(size_t pos) noexcept
    {
        const auto tail = pos % width;
        const auto cnt = pos / width;
        size_t n = length - cnt;
        if (cnt)
            for (size_t i = length; i-- != 0;)
                data.data[i] = data.data[--n];
        for (size_t i = cnt; i-- != 0;)
            data.data[i] = 0;
        n = cnt;
        if (tail)
            for (size_t down = 0, tmp; n != length; ++n) {
                tmp = data.data[n] & (std::numeric_limits<size_t>::max() <<
                                      (width - tail));
                tmp >>= width - tail;
                data.data[n] <<= tail;
                data.data[n] |= down;
                down = tmp;
            }
        data.data[length - 1] &= std::numeric_limits<size_t>::max() >>
            (width - N % width);
        return *this;
    }

    bitset operator<<(size_t pos) const noexcept
    {
        bitset bs(*this);
        return bs <<= pos;
    }

    bitset operator>>(size_t pos) const noexcept
    {
        bitset bs(*this);
        return bs >>= pos;
    }

    template <typename C = char,
              typename T = std::char_traits<char>,
              typename A = allocator<char>>
    basic_string<C, T, A> to_string(C zero = C('0'), C one = C('1'))
    {
        basic_string<C, T, A> s(N, zero);
        for (size_t n = N, i = 0; n-- != 0; ++i)
            if (reference(data, n / width, n % width))
                s[i] = one;
        return s;
    }

    unsigned long to_ulong() const
    {
        for (size_t i = 1; i != length; ++i)
            if (data.data[i])
                throw std::overflow_error("bitset too large");
        return data.data[0];
    }

    unsigned long long to_ullong() const
    {
        size_t i = 2;
        while (i < length)
            if (data.data[i++])
                throw std::overflow_error("bitset too large");
#if SIZE_MAX == ULLONG_MAX
        if (length > 1 && data.data[1])
            throw std::overflow_error("bitset too large");
        return data.data[0];
#else
        return length == 1
            ? (unsigned long long)(data.data[0])
            : (unsigned long long)(data.data[0]) |
                ((unsigned long long)(data.data[1]) << width);
#endif // SIZE_MAX
    }
};

template <size_t N> inline
bitset<N> operator&(const bitset<N> &bs1, const bitset<N> &bs2) noexcept
{
    bitset<N> bs(bs1);
    return bs &= bs2;
}

template <size_t N> inline
bitset<N> operator|(const bitset<N> &bs1, const bitset<N> &bs2) noexcept
{
    bitset<N> bs(bs1);
    return bs |= bs2;
}

template <size_t N> inline
bitset<N> operator^(const bitset<N> &bs1, const bitset<N> &bs2) noexcept
{
    bitset<N> bs(bs1);
    return bs ^= bs2;
}

template <typename C, typename T, size_t N>
std::basic_istream<C, T> &operator>>(std::basic_istream<C, T> &is, bitset<N> &bs)
{
    C c;
    const C zero = is.widen('0'), one = is.widen('1');
    bs = bitset<N>();
    size_t n = N;
    try {
        if (!std::basic_istream<C, T>::sentry(is, true))
            return is;
    } catch (...) {
        return is;
    }
    try {
        for (c = is.rdbuf()->sgetc(); n--; c = is.rdbuf()->snextc()) {
            if (T::eq_int_type(c, T::eof())) {
                is.setstate(std::ios_base::eofbit);
                break;
            }
            if (c == one)
                bs.flip(n);
            else if (c != zero)
                break;
        }
    } catch (...) {
        is.setstate(std::ios_base::badbit);
    }
    if (n == N - 1)
        is.setstate(std::ios_base::failbit);
    else
        bs >>= n;
    return is;
}

template <typename C, typename T, size_t N>
std::basic_ostream<C, T> &operator<<(std::basic_ostream<C, T> &os,
                                     const bitset<N> &bs)
{
    const size_t w = os.width();
    const auto small = N < w;
    const auto fill_left = os.flags() && std::ios_base::left;
    os.width(0);
    unique_ptr<C[]> buf;
    if (small)
        buf.reset(new C(w));
    C str[N], *p = small ? buf.get() : str;
    if (small && !fill_left)
        for (auto f = w - N; f-- != 0;)
            p[f + N] = os.fill();
    const C zero = os.widen('0'), one = os.widen('1');
    for (size_t n = 0, i = small && fill_left ? w : N; n != N; ++n)
        p[--i] = (bs[n] ? one : zero);
    if (small && fill_left)
        for (auto f = w - N; f-- != 0;)
            p[f] = os.fill();
    return os.write(p, (small ? w : N));
}

template <size_t N>
struct hash<bitset<N>>: function_type<size_t (bitset<N>)>, slow_hash_base {
    size_t operator()(const bitset<N> &bs) const noexcept
    {
        constexpr bool f = N % CHAR_BIT;
        return FNVhash<char>::_1a(reinterpret_cast<const char *>(bs.data.data),
                                  N / CHAR_BIT + f);
    }
};

template <>
struct hash<bitset<0>>: function_type<size_t (bitset<0>)>, fast_hash_base {
    size_t operator()(const bitset<0> &bs) const noexcept
        { return 0; }
};

template <size_t, char...> struct bitset_literal_factory;

template <size_t N, char... cs>
struct bitset_literal_factory<N, '0', cs...> {
    static bitset<N> gen() { return bitset_literal_factory<N, cs...>::gen(); }
};

template <size_t N, char... cs>
struct bitset_literal_factory<N, '1', cs...> {
    static bitset<N> gen()
    {
        return bitset_literal_factory<N, cs...>::gen().set(sizeof...(cs));
    }
};

template <size_t N>
struct bitset_literal_factory<N, '0'> {
    static bitset<N> gen() { return bitset<N>(); }
};

template <size_t N>
struct bitset_literal_factory<N, '1'> {
    static bitset<N> gen() { return bitset<N>().set(0); }
};

namespace bitset_literal {
/* big end */
template <char... cs> inline bitset<sizeof...(cs)> operator""_bs()
{
    return bitset_literal_factory<sizeof...(cs), cs...>::gen();
}

} // namespace bitset_literal

} // namespace glglT

#endif // GLGL_BITSET_H

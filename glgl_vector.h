#ifndef GLGL_VECTOR_H
#define GLGL_VECTOR_H

#include <climits>
#include <stdexcept>
#include <initializer_list>
#include "sub/glgl_allocator.h"
#include "sub/glgl_allocator_traits.h"
#include "sub/glgl_base_algo.h"
#include "sub/glgl_hash.h"
#include "sub/glgl_other_iterator.h"

namespace glglT {

template <typename T, typename Alloc = allocator<T>>
class vector {
    constexpr static int SV_LIM = 16;
    struct copy_tag {};
    struct fill_tag {};
    struct move_tag {};
public:
    typedef T value_type;
    typedef Alloc allocator_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef normal_iterator<pointer> iterator;
    typedef normal_iterator<const_pointer> const_iterator;
    typedef glglT::reverse_iterator<pointer> reverse_iterator;
    typedef glglT::reverse_iterator<const_pointer> const_reverse_iterator;
private:
    Alloc a;
    pointer first;
    pointer last;
    pointer tail;
public:
    explicit vector(const Alloc &ac = Alloc()) noexcept(noexcept(Alloc(ac))):
        a(ac), first(nullptr), last(nullptr), tail(nullptr) {}

    vector(size_type n, const T &t, const Alloc &ac = Alloc()): a(ac)
    {
        if (SV_LIM < n)
            this->init(n, n * 2, fill_tag(), t);
        else
            this->init(n, SV_LIM, fill_tag(), t);
    }

    explicit vector(size_type n)
    {
        if (SV_LIM < n)
            this->init(n, n * 2, fill_tag());
        else
            this->init(n, SV_LIM, fill_tag());
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    vector(It b, It e, const Alloc &ac = Alloc()):
        vector(b, e, ac, typename iterator_traits<It>::iterator_category()) {}

    vector(const vector &o):
        a(allocator_traits<Alloc>::
          select_on_container_copy_construction(o.get_allocator()))
    {
        const auto n = o.size();
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), o.first);
        else
            this->init(n, SV_LIM, copy_tag(), o.first);
    }

    vector(const vector &o, const Alloc &ac): a(ac)
    {
        const auto n = o.size();
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), o.first);
        else
            this->init(n, SV_LIM, copy_tag(), o.first);
    }

    vector(vector &&o): a(glglT::move(o.a)),
        first(o.first), last(o.last), tail(o.tail)
    {
        o.first = o.last = o.tail = nullptr;
    }

    vector(vector &&o, const Alloc &ac): a(ac),
        first(o.first), last(o.last), tail(o.tail)
    {
        if (a == o.a)
            o.first = o.last = o.tail = nullptr;
        else {
            const auto n = o.size();
            if (SV_LIM < n)
                this->init(n, n * 2, move_tag(), o.first);
            else
                this->init(n, SV_LIM, move_tag(), o.first);
        }
    }

    vector(std::initializer_list<T> il, const Alloc &ac = Alloc()): a(ac)
    {
        const auto n = il.size();
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), il.begin());
        else
            this->init(n, SV_LIM, copy_tag(), il.begin());
    }

    ~vector() { this->free(); }

    vector &operator=(const vector &o)
    {
        if (first == o.first)
            return *this;
        this->free();
        glglT::alloc_copy(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_copy_assignment());
        const auto n = o.size();
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), o.first);
        else
            this->init(n, SV_LIM, copy_tag(), o.first);
        return *this;
    }

    vector &operator=(vector &&o)
    {
        if (first == o.first)
            return *this;
        this->free();
        glglT::alloc_move(a, glglT::move(o.a), typename allocator_traits<Alloc>::
                          propagate_on_container_move_assignment());
        if (a == o.a) {
            first = o.first;
            last = o.last;
            tail = o.tail;
            o.first = o.last = o.tail = nullptr;
        } else {
            const auto n = o.size();
            if (SV_LIM < n)
                this->init(n, n * 2, move_tag(), o.first);
            else
                this->init(n, SV_LIM, move_tag(), o.first);
        }
        return *this;
    }

    vector &operator=(std::initializer_list<T> il)
    {
        this->assign(il);
        return *this;
    }

    void assign(size_type n, const T &t)
    {
        this->free();
        if (SV_LIM < n)
            this->init(n, n * 2, fill_tag(), t);
        else
            this->init(n, SV_LIM, fill_tag(), t);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    void assign(It b, It e)
    {
        this->assign(b, e, typename iterator_traits<It>::iterator_category());
    }

    void assign(iterator b, iterator e)
    {
        this->assign(const_iterator(b), const_iterator(e));
    }

    void assign(const_iterator b, const_iterator e)
    {
        if (first <= b.base() && b.base() <= last) {
            this->erase(e, const_iterator(last));
            this->erase(const_iterator(first), b);
            return;
        }
        this->free();
        if (b != e) {
            const auto n = glglT::distance(b, e);
            if (SV_LIM < n)
                this->init(n, n * 2, copy_tag(), b);
            else
                this->init(n, SV_LIM, copy_tag(), b);
        }
    }

    void assign(std::initializer_list<T> il)
    {
        this->free();
        const auto n = il.size();
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), il.begin());
        else
            this->init(n, SV_LIM, copy_tag(), il.begin());
    }

    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    T &at(size_type pos)
    {
        if (!(pos < this->size()))
            throw std::out_of_range("vector out of range");
        return first[pos];
    }

    const T &at(size_type pos) const
    {
        if (!(pos < this->size()))
            throw std::out_of_range("vector out of range");
        return first[pos];
    }

    T &operator[](size_type pos) noexcept { return first[pos]; }
    const T &operator[](size_type pos) const noexcept { return first[pos]; }

    T &front() noexcept { return *first; }
    const T &front() const noexcept { return *first; }

    T &back() noexcept { return *(last - 1); }
    const T &back() const noexcept { return *(last - 1); }

    T *data() noexcept { return first; }
    const T *data() const noexcept { return first; }

    iterator begin() noexcept { return iterator(first); }
    const_iterator begin() const noexcept { return const_iterator(first); }
    const_iterator cbegin() const noexcept { return const_iterator(first); }

    iterator end() noexcept { return iterator(last); }
    const_iterator end() const noexcept { return const_iterator(last); }
    const_iterator cend() const noexcept { return const_iterator(last); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(last); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(last); }
    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(last); }

    reverse_iterator rend() noexcept { return reverse_iterator(first); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(first); }
    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(first); }

    bool empty() const noexcept { return first == last; }
    size_type size() const noexcept { return last - first; }
    size_type capacity() const noexcept { return tail - first; }
    constexpr size_type max_size() const noexcept
    {
        return allocator_traits<Alloc>::max_size(a);
    }

    void shrink_to_fit() { this->move_area(this->size()); }
    void reserve(size_type new_cap)
    {
        if (this->capacity() < new_cap) {
            if (max_size() < new_cap)
                throw std::length_error("vector too large");
            this->move_area(new_cap);
        }
    }


    void clear() noexcept
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, first, this->size());
        last = first;
    }

    iterator insert(const_iterator itr, const T &t)
    {
        const auto pos = itr.base() - first;
        this->push_back_aux(t);
        const auto pos_1 = pos - 1;
        for (auto i = this->size() - 1; --i != pos_1;)
            glglT::iter_swap(first + i, first + i + 1);
        return iterator(first + pos);
    }

    iterator insert(const_iterator itr, T &&t)
    {
        const auto pos = itr.base() - first;
        this->push_back_aux(glglT::move(t));
        const auto pos_1 = pos - 1;
        for (auto i = this->size() - 1; --i != pos_1;)
            glglT::iter_swap(first + i, first + i + 1);
        return iterator(first + pos);
    }

    iterator insert(const_iterator itr, size_type n, const T &t)
    {
        const auto pos = itr.base() - first;
        const auto sz = this->size();
        if (this->capacity() < sz + n)
            this->insr(n, sz, pos, fill_tag(), t);
        else
            this->ins2(n, sz, pos, fill_tag(), t);
        return iterator(first + pos);
    }

    template <typename It, typename = typename
        enable_if<!is_integral<It>::value, It>::type>
    iterator insert(const_iterator itr, It b, It e)
    {
        return this->insert(itr, b, e, typename iterator_traits<It>::
                            iterator_category());
    }

    iterator insert(const_iterator itr, std::initializer_list<T> il)
    {
        const auto pos = itr.base() - first;
        const auto sz = this->size();
        const auto n = il.size();
        const auto b = il.begin();
        if (this->capacity() < sz + n)
            this->insr(n, sz, pos, copy_tag(), b);
        else
            this->ins2(n, sz, pos, copy_tag(), b);
        return iterator(first + pos);
    }

    template <typename... Args>
    iterator emplace(const_iterator itr, Args&&... args)
    {
        const auto pos = itr.base() - first;
        this->push_back_aux(glglT::forward<Args>(args)...);
        const auto pos_1 = pos - 1;
        for (auto i = this->size() - 1; --i != pos_1;)
            glglT::iter_swap(first + i, first + i + 1);
        return iterator(first + pos);
    }

    void push_back(const T &t) { this->push_back_aux(t); }
    void push_back(T &&t) { this->push_back_aux(glglT::move(t)); }

    template <typename... Args>
    void emplace_back(Args&&... args)
    {
        this->push_back_aux(glglT::forward<Args>(args)...);
    }

    void pop_back() noexcept
    {
        allocator_traits<Alloc>::destroy(a, --last);
    }

    void resize(size_type n)
    {
        const auto sz = this->size();
        if (n < sz)
            this->erase(this->begin() + n, this->end());
        else
            while (n-- != 0)
                this->emplace_back();
    }

    void resize(size_type n, const T &t)
    {
        const auto sz = this->size();
        if (n < sz)
            this->erase(this->begin() + n, this->end());
        else
            this->insert(this->end(), n - sz, t);
    }

    void swap(vector &o)
    {
        glglT::alloc_swap(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_swap());
        glglT::swap(first, o.first);
        glglT::swap(last, o.last);
        glglT::swap(tail, o.tail);
    }

    iterator erase(const_iterator itr)
    {
        if (itr.base() == last)
            return iterator(last);
        pointer it = first + (itr.base() - first);
        glglT::move(it + 1, last, it);
        allocator_traits<Alloc>::destroy(a, --last);
        return iterator(it);
    }

    iterator erase(const_iterator b, const_iterator e)
    {
        const auto n = e - b;
        pointer it = first + (b.base() - first);
        last = glglT::move(it + n, last, it);
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            destroy(a, last, n);
        return iterator(it);
    }
private:
    void free() noexcept
    {
        if (first != nullptr) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first, last - first);
            allocator_traits<Alloc>::deallocate(a, first,
                                                alloc_de_size(a, first, tail));
        }
    }

    void move_area(size_type new_cap)
    {
        const auto sz = this->size();
        const auto of = first;
        try {
            first = allocator_traits<Alloc>::allocate(a, new_cap);
        } catch(...) {
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of, sz, first);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, new_cap);
            first = of;
            throw;
        }
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::destroy(a, of, sz);
        allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
        last = first + sz;
        tail = first + new_cap;
    }

    template <typename It>
    vector(It b, It e, const Alloc &ac, input_iterator_tag): a(ac)
    {
        if (b != e) {
            this->init(1, SV_LIM, fill_tag(), *b);
            while (++b != e)
                this->push_back(*b);
        } else
            first = last = tail = nullptr;
    }

    template <typename It>
    vector(It b, It e, const Alloc &ac, forward_iterator_tag): a(ac)
    {
        const auto n = glglT::distance(b, e);
        if (SV_LIM < n)
            this->init(n, n * 2, copy_tag(), b);
        else
            this->init(n, SV_LIM, copy_tag(), b);
    }

    template <typename It>
    void assign(It b, It e, input_iterator_tag)
    {
        this->free();
        if (b != e) {
            this->init(1, SV_LIM, fill_tag(), *b);
            while (++b != e)
                push_back(*b);
        }
    }

    template <typename It>
    void assign(It b, It e, forward_iterator_tag)
    {
        this->free();
        if (b != e) {
            const auto n = glglT::distance(b, e);
            if (SV_LIM < n)
                this->init(n, n * 2, copy_tag(), b);
            else
                this->init(n, SV_LIM, copy_tag(), b);
        }
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, input_iterator_tag)
    {
        const auto pos = itr.base() - first;
        for (; b != e; ++itr, ++b)
            itr = this->insert(itr, 1, *b);
        return iterator(first + pos);
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, forward_iterator_tag)
    {
        const auto pos = itr.base() - first;
        const auto sz = this->size();
        const auto n = glglT::distance(b, e);
        if (this->capacity() < sz + n)
            this->insr(n, sz, pos, copy_tag(), b);
        else
            this->ins2(n, sz, pos, copy_tag(), b);
        return iterator(first + pos);
    }

    template <typename... Args>
    void inner_new_place_construct(size_type n, pointer where,
                                   fill_tag, Args&&... args)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_fill_n(a, where, n, glglT::forward<Args>(args)...);
    }

    template <typename It>
    void inner_new_place_construct(size_type n, pointer where, copy_tag, It b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, b, n, where);
    }

    void inner_new_place_construct(size_type n, pointer where, move_tag, pointer b)
    {
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_move_n(a, b, n, where);
    }

    template <typename... Args>
    void init(size_type n, size_type cap, Args&&... args)
    {
        try {
            first = allocator_traits<Alloc>::allocate(a, cap);
        } catch(...) {
            first = last = tail = nullptr;
            throw;
        }
        try {
            this->inner_new_place_construct(n, first, glglT::forward<Args>(args)...);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, cap);
            first = last = tail = nullptr;
            throw;
        }
        last = first + n;
        tail = first + cap;
    }

    template <typename... Args>
    void insr(size_type n, size_type sz, size_type pos, Args&&... args)
    {
        const auto new_cap = sz == 0 ? (SV_LIM >= n ? SV_LIM : n * 2)
                                     :  sz < n ? sz + n : sz * 2;
        const auto of = first;
        try {
            first = allocator_traits<Alloc>::allocate(a, new_cap);
        } catch(...) {
            first = of;
            throw;
        }
        try {
            this->inner_new_place_construct(n, first + pos, glglT::forward<Args>(args)...);
        } catch(...) {
            allocator_traits<Alloc>::deallocate(a, first, new_cap);
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of + pos, sz - pos, first + n + pos);
        } catch(...) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first + pos, n);
            allocator_traits<Alloc>::deallocate(a, first, new_cap);
            first = of;
            throw;
        }
        try {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                uninitialized_move_n(a, of, pos, first);
        } catch(...) {
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, first + pos, n);
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy_move(a, first + n + pos, of + pos, sz - pos);
            allocator_traits<Alloc>::deallocate(a, first, new_cap);
            first = of;
            throw;
        }
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::destroy(a, of, sz);
        allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
        last = first + sz + n;
        tail = first + new_cap;
    }

    template <typename It>
    void inner_ins2(size_type n, pointer where, copy_tag, It b)
    {
        glglT::copy_n(b, n, where);
    }

    template <typename Arg>
    void inner_ins2(size_type n, pointer where, fill_tag, Arg &&arg)
    {
        glglT::fill_n(where, n, glglT::forward<Arg>(arg));
    }

    template <typename... Args>
    void ins2(size_type n, size_type sz, size_type pos, Args&&... args)
    {
        const auto beflast = last - n;
        uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
            uninitialized_copy_n(a, beflast, n, last);
        const auto ipos = first + pos;
        if (ipos < beflast)
            try {
                glglT::move_backward(ipos, beflast, last);
            } catch(...) {
                last += n;
                throw;
            }
        try {
            this->inner_ins2(n, ipos, glglT::forward<Args>(args)...);
        } catch(...) {
            glglT::move(ipos + n, last + n, ipos);
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::destroy(a, last, n);
            throw;
        }
        last += n;
    }

    template <typename... Args>
    void push_back_aux(Args&&... args)
    {
        const auto sz = this->size();
        if (sz != this->capacity()) {
            allocator_traits<Alloc>::construct(a, last, glglT::forward<Args>(args)...);
            ++last;
        } else {
            const auto new_cap = sz == 0 ? SV_LIM : 2 * sz;
            auto of = first;
            try {
                first = allocator_traits<Alloc>::allocate(a, new_cap);
            } catch(...) {
                first = of;
                throw;
            }
            try {
                allocator_traits<Alloc>::construct(a, first + sz,
                                                   glglT::forward<Args>(args)...);
            } catch(...) {
                allocator_traits<Alloc>::deallocate(a, first, new_cap);
                first = of;
                throw;
            }
            try {
                uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                    uninitialized_move_n(a, of, sz, first);
            } catch(...) {
                allocator_traits<Alloc>::destroy(a, first + sz);
                allocator_traits<Alloc>::deallocate(a, first, new_cap);
                first = of;
                throw;
            }
            uninitialized_with_alloc<Alloc, allocator_traits<Alloc>>::
                destroy(a, of, sz);
            allocator_traits<Alloc>::deallocate(a, of, alloc_de_size(a, of, tail));
            last = first + sz + 1;
            tail = first + new_cap;
        }
    }

#undef GLGL_VECTOR_USE_MOVE_AERA
};

template <typename T, typename A> inline
void swap(vector<T, A> &v1, vector<T, A> &v2)
{
    v1.swap(v2);
}

template <typename T, typename A>
bool operator==(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    for (auto it1 = v1.begin(), it2 = v2.begin(); ; ++it1, ++it2) {
        if (it1 == v1.end())
            return it2 == v2.end();
        if (it2 == v2.end())
            return false;
        if (*it1 != *it2)
            return false;
    }   return true;
}

template <typename T, typename A> inline
bool operator!=(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    return !(v1 == v2);
}

template <typename T, typename A>
bool operator<(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    for (auto it1 = v1.begin(), it2 = v2.begin(); ; ++it1, ++it2) {
        if (it1 == v1.end())
            return true;
        if (it2 == v2.end())
            return false;
        if (*it1 < *it2)
            return true;
        if (*it2 < *it1)
            return false;
    }
}

template <typename T, typename A> inline
bool operator>(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    return (v2 < v1);
}

template <typename T, typename A> inline
bool operator>=(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    return !(v1 < v2);
}

template <typename T, typename A> inline
bool operator<=(const vector<T, A> &v1, const vector<T, A> &v2) noexcept
{
    return !(v2 < v1);
}

class bool_vector_pointer;
class bool_vector_const_pointer;

class bool_vector_reference {
    template <typename, typename> friend class vector;
    template <typename> friend class hash;
    friend class bool_vector_pointer;
    friend class bool_vector_const_pointer;
    size_t *ptr;
    size_t bitw;
    constexpr bool_vector_reference() noexcept: ptr(nullptr), bitw(0) {}
    bool_vector_reference(size_t *p, size_t w) noexcept: ptr(p), bitw(w) {}

    bool_vector_reference &operator=(bool b) noexcept
    {
        const auto mask = size_t(1) << bitw;
        if (b) *ptr |= mask;
        else *ptr &= ~mask;
        return *this;
    }

    operator bool () const noexcept
    {
        return *ptr & (size_t(1) << bitw);
    }

    void flip() noexcept
    {
        const auto mask = size_t(1) << bitw;
        *ptr ^= mask;
    }
public:
    struct proxy;
    friend class proxy;
};

struct bool_vector_reference::proxy {
    bool_vector_reference r;
    explicit proxy(const bool_vector_reference &o): r(o) {}
    proxy(const proxy &) = default;
    proxy &operator=(const proxy &o) noexcept
    {
        r = o;
        return *this;
    }

    proxy &operator=(bool b) noexcept
    {
        r = b;
        return *this;
    }

    operator bool () const noexcept { return r; }
    void flip() noexcept { r.flip(); }
};

class bool_vector_pointer;

class bool_vector_const_pointer: public iterator<random_access_iterator_tag, bool> {
    template <typename, typename> friend class vector;
    template <typename> friend class hash;
    friend class bool_vector_pointer;
    constexpr static size_t width = sizeof(size_t) * CHAR_BIT;

    bool_vector_reference r;
    constexpr bool_vector_const_pointer() noexcept = default;
    explicit bool_vector_const_pointer(bool_vector_reference rf) noexcept: r(rf) {}
public:
    bool operator*() const noexcept { return r; }
    bool operator[](ptrdiff_t n) const noexcept { return (*this + n).r; }
    explicit operator bool_vector_pointer () const noexcept;

    bool_vector_const_pointer &operator++() noexcept
    {
        if (++r.bitw == width)
            ++r.ptr, r.bitw = 0;
        return *this;
    }

    bool_vector_const_pointer operator++(int) noexcept
    {
        bool_vector_const_pointer p(*this);
        this->operator++();
        return p;
    }

    bool_vector_const_pointer &operator--() noexcept
    {
        if (r.bitw == 0)
            --r.ptr, r.bitw = width - 1;
        else
            --r.bitw;
        return *this;
    }

    bool_vector_const_pointer operator--(int) noexcept
    {
        bool_vector_const_pointer p(*this);
        this->operator--();
        return p;
    }

    bool_vector_const_pointer &operator+=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator-=(-n);
        r.bitw += n % width;
        r.ptr += n / width;
        if (r.bitw >= width)
            ++r.ptr, r.bitw -= width;
        return *this;
    }

    bool_vector_const_pointer operator+(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator-(-n);
        size_t t_bit = r.bitw + n % width;
        size_t *t_ptr = r.ptr + n / width;
        if (t_bit >= width)
            ++t_ptr, t_bit -= width;
        return bool_vector_const_pointer(bool_vector_reference(t_ptr, t_bit));
    }

    bool_vector_const_pointer &operator-=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator+=(-n);
        r.bitw -= n % width;
        r.ptr -= n / width;
        if (r.bitw > width)
            --r.ptr, r.bitw += width;
        return *this;
    }

    bool_vector_const_pointer operator-(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator+(-n);
        size_t t_bit = r.bitw - n % width;
        size_t *t_ptr = r.ptr - n / width;
        if (t_bit > width)
            --t_ptr, t_bit += width;
        return bool_vector_const_pointer(bool_vector_reference(t_ptr, t_bit));
    }

    ptrdiff_t operator-(const bool_vector_const_pointer &p) const noexcept
    {
        return width * (r.ptr - p.r.ptr) + r.bitw - p.r.bitw;
    }

    bool operator<(const bool_vector_const_pointer &p) const noexcept
    {
        return r.ptr < p.r.ptr || (r.ptr == p.r.ptr && r.bitw < p.r.bitw);
    }

    bool operator==(const bool_vector_const_pointer &p) const noexcept
    {
        return r.ptr == p.r.ptr && r.bitw == p.r.bitw;
    }
};

inline bool operator>(const bool_vector_const_pointer &p1,
                      const bool_vector_const_pointer &p2) noexcept
{
    return p2 < p1;
}

inline bool operator<=(const bool_vector_const_pointer &p1,
                       const bool_vector_const_pointer &p2) noexcept
{
    return !(p1 > p2);
}

inline bool operator>=(const bool_vector_const_pointer &p1,
                       const bool_vector_const_pointer &p2) noexcept
{
    return !(p1 < p2);
}

inline bool operator!=(const bool_vector_const_pointer &p1,
                       const bool_vector_const_pointer &p2) noexcept
{
    return !(p1 == p2);
}

inline
bool_vector_const_pointer operator+(ptrdiff_t n,
                                    const bool_vector_const_pointer &p) noexcept
{
    return p + n;
}

class bool_vector_pointer: public iterator<random_access_iterator_tag, bool> {
    template <typename, typename> friend class vector;
    friend class bool_vector_const_pointer;
    constexpr static size_t width = sizeof(size_t) * CHAR_BIT;

    bool_vector_reference r;
    constexpr bool_vector_pointer() noexcept = default;
    explicit bool_vector_pointer(bool_vector_reference rf) noexcept: r(rf) {}
public:
    bool_vector_reference::proxy operator*() const noexcept
    {
        return bool_vector_reference::proxy(r);
    }

    operator bool_vector_const_pointer () const noexcept
    {
        return bool_vector_const_pointer(r);
    }

    bool_vector_pointer &operator++() noexcept
    {
        if (++r.bitw == width)
            ++r.ptr, r.bitw = 0;
        return *this;
    }

    bool_vector_pointer operator++(int) noexcept
    {
        bool_vector_pointer p(*this);
        this->operator++();
        return p;
    }

    bool_vector_pointer &operator--() noexcept
    {
        if (r.bitw == 0)
            --r.ptr, r.bitw = width - 1;
        else
            --r.bitw;
        return *this;
    }

    bool_vector_pointer operator--(int) noexcept
    {
        bool_vector_pointer p(*this);
        this->operator--();
        return p;
    }

    bool_vector_pointer &operator+=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator-=(-n);
        r.bitw += n % width;
        r.ptr += n / width;
        if (r.bitw >= width)
            ++r.ptr, r.bitw -= width;
        return *this;
    }

    bool_vector_pointer operator+(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator-(-n);
        size_t t_bit = r.bitw + n % width;
        size_t *t_ptr = r.ptr + n / width;
        if (t_bit >= width)
            ++t_ptr, t_bit -= width;
        return bool_vector_pointer(bool_vector_reference(t_ptr, t_bit));
    }

    bool_vector_pointer &operator-=(ptrdiff_t n) noexcept
    {
        if (n < 0)
            return this->operator+=(-n);
        r.bitw -= n % width;
        r.ptr -= n / width;
        if (r.bitw > width)
            --r.ptr, r.bitw += width;
        return *this;
    }

    bool_vector_pointer operator-(ptrdiff_t n) const noexcept
    {
        if (n < 0)
            return this->operator+(-n);
        size_t t_bit = r.bitw - n % width;
        size_t *t_ptr = r.ptr - n / width;
        if (t_bit > width)
            --t_ptr, t_bit += width;
        return bool_vector_pointer(bool_vector_reference(t_ptr, t_bit));
    }

    ptrdiff_t operator-(const bool_vector_pointer &p) const noexcept
    {
        return width * (r.ptr - p.r.ptr) + r.bitw - p.r.bitw;
    }

    ptrdiff_t operator-(const bool_vector_const_pointer &p) const noexcept
    {
        return this->operator-(static_cast<bool_vector_pointer>(p));
    }

    bool operator<(const bool_vector_const_pointer &p) const noexcept
    {
        return r.ptr < p.r.ptr || (r.ptr == p.r.ptr && r.bitw < p.r.bitw);
    }

    bool operator==(const bool_vector_const_pointer &p) const noexcept
    {
        return r.ptr == p.r.ptr && r.bitw == p.r.bitw;
    }

    bool_vector_reference::proxy operator[](ptrdiff_t n) const noexcept
    {
        return bool_vector_reference::proxy((*this + n).r);
    }
};

inline bool operator>(const bool_vector_pointer &p1,
                      const bool_vector_pointer &p2) noexcept
{
    return p2 < p1;
}

inline bool operator<=(const bool_vector_pointer &p1,
                       const bool_vector_pointer &p2) noexcept
{
    return !(p1 > p2);
}

inline bool operator>=(const bool_vector_pointer &p1,
                       const bool_vector_pointer &p2) noexcept
{
    return !(p1 < p2);
}

inline bool operator!=(const bool_vector_pointer &p1,
                       const bool_vector_pointer &p2) noexcept
{
    return !(p1 == p2);
}

inline bool_vector_pointer operator+(ptrdiff_t n,
                                     const bool_vector_pointer &p) noexcept
{
    return p + n;
}

inline
bool_vector_const_pointer::operator bool_vector_pointer () const noexcept
{
    return bool_vector_pointer(r);
}

template <typename Alloc>
class vector<bool, Alloc> {
public:
    typedef bool value_type;
    typedef Alloc allocator_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef bool_vector_reference::proxy reference;
    typedef bool const_reference;
    typedef bool_vector_pointer pointer;
    typedef bool_vector_const_pointer const_pointer;
    typedef pointer iterator;
    typedef const_pointer const_iterator;
    typedef glglT::reverse_iterator<pointer> reverse_iterator;
    typedef glglT::reverse_iterator<const_pointer> const_reverse_iterator;
private:
    typedef typename allocator_traits<Alloc>::
        template rebind_alloc<size_t> rebind_alloc;
    typedef typename allocator_traits<Alloc>::
        template rebind_traits<size_t> rebind_traits;
    constexpr static size_t width = sizeof(size_t) * CHAR_BIT;

    rebind_alloc a;
    pointer first;
    pointer last;
    pointer tail;
public:
    explicit vector(const Alloc &ac = Alloc())
        noexcept(noexcept(rebind_alloc(ac))): a(ac),
        first(bool_vector_reference(nullptr, 0)),
        last(bool_vector_reference(nullptr, 0)),
        tail(bool_vector_reference(nullptr, 0)) {}

    explicit vector(size_type n)
    {
        const auto cap = width < n ? 2 * (n / width) : 1;
        const auto sz = n / width + bool(n % width);
        this->init(n, cap, sz, size_t(0));
    }

    vector(size_type n, const bool &b, const Alloc &ac = Alloc()): a(ac)
    {
        const auto cap = width < n ? 2 * (n / width) : 1;
        const auto sz = n / width + bool(n % width);
        const size_t val = b ? std::numeric_limits<size_t>::max() : 0;
        this->init(n, cap, sz, val);
    }

    template <typename It, typename = typename
        enable_if<!is_integral<It>::value, It>::type>
    vector(It b, It e, const Alloc &ac = Alloc()):
        vector(b, e, ac, typename iterator_traits<It>::iterator_category()) {}

    vector(const vector &o, const Alloc &ac): a(ac)
    {
        const auto n = o.size();
        const auto cap = width < n ? 2 * (n / width) : 1;
        const auto sz = n / width + bool(n % width);
        this->init(n, cap, sz, o.first.r.ptr);
    }

    vector(const vector &o):
        vector(o, rebind_traits::select_on_container_copy_construction(o.a)) {}

    vector(vector &&o)
        noexcept(noexcept(rebind_alloc(declval<rebind_alloc &&>()))): a(o.a),
        first(o.first), last(o.last), tail(o.tail)
    {
        o.first.r = bool_vector_reference(nullptr, 0);
        o.last.r = bool_vector_reference(nullptr, 0);
        o.tail.r = bool_vector_reference(nullptr, 0);
    }

    vector(vector &&o, const Alloc &ac)
        noexcept(noexcept(rebind_alloc(ac))): a(ac),
        first(o.first), last(o.last), tail(o.tail)
    {
        if (a == o.a) {
            o.first.r = bool_vector_reference(nullptr, 0);
            o.last.r = bool_vector_reference(nullptr, 0);
            o.tail.r = bool_vector_reference(nullptr, 0);
        } else {
            const auto n = o.size();
            const auto cap = width < n ? 2 * (n / width) : 1;
            const auto sz = n / width + bool(n / width);
            this->init(n, cap, sz, o.first.r.ptr);
        }
    }

    vector(std::initializer_list<bool> il, const Alloc &ac = Alloc()): a(ac)
    {
        this->init(il.size(), il.begin(), il.end());
    }

    ~vector() { this->free(); }

    vector &operator=(const vector &o)
    {
        if (first == o.first)
            return *this;
        this->free();
        alloc_copy(a, o.a, typename rebind_traits::
                   propagate_on_container_copy_assignment());
        const auto n = o.size();
        const auto cap = width < n ? 2 * (n / width) : 1;
        const auto sz = n / width + bool(n / width);
        this->init(n, cap, sz, o.first.r.ptr);
        return *this;
    }

    vector &operator=(vector &&o)
    {
        if (first == o.first)
            return *this;
        this->free();
        alloc_move(a, glglT::move(o.a), typename rebind_traits::
                   propagate_on_container_move_assignment());
        if (a == o.a) {
            first = o.first;
            last = o.last;
            tail = o.tail;
            o.first.r = bool_vector_reference(nullptr, 0);
            o.last.r = bool_vector_reference(nullptr, 0);
            o.tail.r = bool_vector_reference(nullptr, 0);
        } else {
            const auto n = o.size();
            const auto cap = width < n ? 2 * (n / width) : 1;
            const auto sz = n / width + bool(n % width);
            this->init(n, cap, sz, o.first.r.ptr);
        }
        return *this;
    }

    vector &operator=(std::initializer_list<bool> il)
    {
        this->free();
        this->init(il.size(), il.begin(), il.end());
        return *this;
    }

    void assign(size_type n, const bool &b)
    {
        this->free();
        const auto cap = width < n ? 2 * (n / width) : 1;
        const auto sz = n / width + bool(n % width);
        const size_t val = b ? std::numeric_limits<size_t>::max() : 0;
        this->init(n, cap, sz, val);
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    void assign(It b, It e)
    {
        assign(b, e, typename iterator_traits<It>::iterator_category());
    }

    Alloc get_allocator() const { return a; }

    reference at(size_type pos)
    {
        if (!(pos < this->size()))
            throw std::out_of_range("vector out of range");
        return first[pos];
    }

    const_reference at(size_type pos) const
    {
        if (!(pos < this->size()))
            throw std::out_of_range("vector out of range");
        return first[pos];
    }

    reference operator[](size_type pos) noexcept { return first[pos]; }
    const_reference operator[](size_type pos) const noexcept { return first[pos]; }

    reference front() noexcept { return *first; }
    const_reference front() const noexcept { return *first; }

    reference back() noexcept { return *(last - 1); }
    const_reference back() const noexcept { return *(last - 1); }

    pointer data() noexcept { return first; }
    const_pointer data() const noexcept { return first; }

    iterator begin() noexcept { return iterator(first); }
    const_iterator begin() const noexcept { return const_iterator(first); }
    const_iterator cbegin() const noexcept { return const_iterator(first); }

    iterator end() noexcept { return iterator(last); }
    const_iterator end() const noexcept { return const_iterator(last); }
    const_iterator cend() const noexcept { return const_iterator(last); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(last); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(last); }
    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(last); }

    reverse_iterator rend() noexcept { return reverse_iterator(first); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(first); }
    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(first); }

    bool empty() const noexcept { return first == last; }
    size_type size() const noexcept { return last - first; }
    size_type capacity() const noexcept { return tail - first; }
    size_type max_size() const noexcept
    {
        return rebind_traits::max_size(a);
    }

    void reserve(size_type cap)
    {
        if (this->capacity() < cap) {
            if (max_size() < cap)
                throw std::length_error("vector too large");
            this->move_area(this->size(), cap);
        }
    }

    void clear() noexcept { last = first; }
    void shrink_to_fit()
    {
        const auto n = this->size();
        this->move_area(n, n / width + bool(n % width));
    }

    iterator insert(const_iterator itr, bool b) { return insert(itr, 1, b); }

    iterator insert(const_iterator itr, size_type n, bool b)
    {
        const auto f_pos = itr - first;
        const auto f_sz = this->size();
        const auto sdiff = f_sz % width;
        const auto sz = f_sz / width + bool(sdiff);
        if (this->capacity() < f_sz + n) {
            const auto real_n = n / width + bool(n % width);
            const auto cap = sz == 0 ? real_n : real_n < sz ? sz * 2 : sz + real_n;
            const auto of = first.r.ptr;
            try {
                first.r.ptr = rebind_traits::allocate(a, cap);
            } catch(...) {
                first.r.ptr = of;
                throw;
            }
            glglT::copy_n(of, sz, first.r.ptr);
            rebind_traits::deallocate(a, of, alloc_de_size(a, of, tail.r.ptr));
            last = first + f_sz + n;
            tail = pointer(bool_vector_reference(first.r.ptr + cap, 0));
        } else
            last += n;
        const auto iter_pos = first + f_pos;
        glglT::move_backward(iter_pos, first + f_sz, last);
        glglT::fill_n(iter_pos, n, b);
        return iter_pos;
    }

    template <typename It, typename = typename
              enable_if<!is_integral<It>::value, It>::type>
    iterator insert(const_iterator itr, It b, It e)
    {
        return this->insert(itr, b, e, typename iterator_traits<It>::
                            iterator_category());
    }

    iterator insert(const_iterator itr, std::initializer_list<bool> il)
    {
        const auto f_pos = itr - first;
        const auto f_sz = this->size();
        const auto b = il.begin();
        const auto n = il.size();
        if (this->capacity() < f_sz + n) {
            return this->insr(n, f_sz, f_pos, b);
        } else
            return this->ins2(n, f_sz, f_pos, b);
    }

    iterator erase(const_iterator itr)
    {
        if (itr == last)
            return last;
        pointer p(itr.r);
        last = glglT::move(p + 1, last, p);
        return p;
    }

    iterator erase(const_iterator b, const_iterator e)
    {
        const auto n = glglT::distance(b, e);
        last = glglT::move(pointer(e.r), last, pointer(b.r));
        return pointer(b.r);
    }

    void push_back(bool b)
    {
        if (last.r.ptr == tail.r.ptr) {
            const auto sz = last.r.ptr - first.r.ptr;
            const auto cap = sz == 0 ? 1 : sz * 2;
            const auto of = first.r.ptr;
            try {
                first.r.ptr = rebind_traits::allocate(a, cap);
            } catch(...) {
                first.r.ptr = of;
                throw;
            }
            last.r.ptr = glglT::copy_n(of, sz, first.r.ptr);
            rebind_traits::deallocate(a, of, alloc_de_size(a, of, tail.r.ptr));
            tail.r.ptr = first.r.ptr + cap;
        }
        *last = b;
        ++last;
    }

    void pop_back() { --last; }

    void resize(size_type n, bool b = false)
    {
        const auto sz = this->size();
        if (n < sz) {
            n = sz - n;
            do {
                this->pop_back();
            } while (--n != 0);
        } else {
            n -= sz;
            while (n-- != 0)
                this->push_back(b);
        }
    }

    void swap(vector &o)
    {
        glglT::alloc_swap(a, o.a, typename allocator_traits<Alloc>::
                          propagate_on_container_swap());
        glglT::swap(first.r, o.first.r);
        glglT::swap(last.r, o.last.r);
        glglT::swap(tail.r, o.tail.r);
    }

    void flip()
    {
        for (pointer p = first; p != last; ++p)
            p.r.flip();
    }

    static void swap(reference r1, reference r2) noexcept
    {
        bool b = r1;
        r1 = bool(r2);
        r2 = b;
    }
private:
    void free() noexcept
    {
        if (first.r.ptr != nullptr) {
            rebind_traits::deallocate(a, first.r.ptr,
                                      alloc_de_size(a, first.r.ptr, tail.r.ptr));
        }
    }

    void move_area(size_type n, size_type cap)
    {
        const auto sz = n / width + bool(n % width);
        const auto of = first.r.ptr;
        try {
            first.r.ptr = rebind_traits::allocate(a, cap);
        } catch(...) {
            first.r.ptr = of;
            throw;
        }
        glglT::copy_n(of, sz, first.r.ptr);
        rebind_traits::deallocate(a, of, alloc_de_size(a, of, tail.r.ptr));
        last = pointer(reference(first.r.ptr + n / width, n % width));
        tail = pointer(reference(first.r.ptr + cap, 0));
    }

    template <typename It>
    vector(It b, It e, const Alloc &ac, input_iterator_tag):
        a(ac), first(bool_vector_reference(nullptr, 0)),
        last(bool_vector_reference(nullptr, 0)), tail(bool_vector_reference(nullptr, 0))
    {
        for (; b != e; ++b)
            this->push_back(*b);
    }

    template <typename It>
    vector(It b, It e, const Alloc &ac, forward_iterator_tag): a(ac)
    {
        const auto n = glglT::distance(b, e);
        this->init(n, b, e);
    }

    template <typename It>
    void assign(It b, It e, input_iterator_tag)
    {
        this->free();
        first = last = tail = pointer(bool_vector_reference(nullptr, 0));
        for (; b != e; ++b)
            this->push_back(*b);
    }

    template <typename It>
    void assign(It b, It e, forward_iterator_tag)
    {
        this->free();
        const auto n = glglT::distance(b, e);
        this->init(n, b, e);
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, input_iterator_tag)
    {
        const auto old_sz = this->size();
        for (; b != e; ++b)
            this->push_back(*b);
        glglT::rotate(iterator(itr.r), first + old_sz, last);
        return iterator(itr.r);
    }

    template <typename It>
    iterator insert(const_iterator itr, It b, It e, forward_iterator_tag)
    {
        const auto f_pos = itr - first;
        const auto f_sz = this->size();
        const auto n = glglT::distance(b, e);
        if (this->capacity() < f_sz + n) {
            return this->insr(n, f_sz, f_pos, b);
        } else
            return this->ins2(n, f_sz, f_pos, b);
    }

    template <typename It>
    void init(size_type n, It b, It e)
    {
        const auto cap = width < n ? 2 * (n / width) : 1;
        size_t *p;
        try {
            p = rebind_traits::allocate(a, cap);
        } catch(...) {
            first = last = tail = pointer(bool_vector_reference(nullptr, 0));
            throw;
        }
        size_t mask = 0;
        p[0] = 0;
        for (size_t *itr = p; b != e; ++b) {
            *itr |= size_t(*b) << mask;
            if (++mask == width) {
                *++itr = 0;
                mask = 0;
            }
        }
        first = pointer(bool_vector_reference(p, 0));
        last = pointer(bool_vector_reference(p + n / width, n % width));
        tail = pointer(bool_vector_reference(p + cap, 0));
    }

    void init(size_type n, size_type cap, size_type sz, const size_t &val)
    {
        size_t *p;
        try {
            p = rebind_traits::allocate(a, cap);
        } catch(...) {
            first = last = tail = pointer(bool_vector_reference(nullptr, 0));
            throw;
        }
        glglT::fill_n(p, sz, val);
        first = pointer(bool_vector_reference(p, 0));
        last = pointer(bool_vector_reference(p + n / width, n % width));
        tail = pointer(bool_vector_reference(p + cap, 0));
    }

    void init(size_type n, size_type cap, size_type sz, const size_t *op)
    {
        size_t *p;
        try {
            p = rebind_traits::allocate(a, cap);
        } catch(...) {
            first = last = tail = pointer(bool_vector_reference(nullptr, 0));
            throw;
        }
        glglT::copy_n(op, sz, p);
        first = pointer(bool_vector_reference(p, 0));
        last = pointer(bool_vector_reference(p + n / width, n % width));
        tail = pointer(bool_vector_reference(p + cap, 0));
    }

    template <typename It>
    iterator insr(size_type n, size_type f_sz, size_type f_pos, It b)
    {
        const auto sdiff = f_sz % width;
        const auto sz = f_sz / width + bool(sdiff);
        const auto real_n = n / width + bool(n % width);
        const auto cap = sz == 0 ? real_n : real_n < sz ? sz * 2 : sz + real_n;
        const auto of = first.r.ptr;
        try {
            first.r.ptr = rebind_traits::allocate(a, cap);
        } catch(...) {
            first.r.ptr = of;
            throw;
        }
        glglT::copy_n(of, sz, first.r.ptr);
        rebind_traits::deallocate(a, of, alloc_de_size(a, of, tail.r.ptr));
        last = first + f_sz + n;
        tail = pointer(bool_vector_reference(first.r.ptr + cap, 0));
        const auto iter_pos = first + f_pos;
        glglT::move_backward(iter_pos, first + f_sz, last);
        glglT::copy_n(b, n, iter_pos);
        return iter_pos;
    }

    template <typename It>
    iterator ins2(size_type n, size_type f_sz, size_type f_pos, It b)
    {
        last += n;
        const auto iter_pos = first + f_pos;
        glglT::move_backward(iter_pos, first + f_sz, last);
        glglT::copy_n(b, n, iter_pos);
        return iter_pos;
    }
#undef GLGL_VECTOR_USE_MOVE_AERA
};

template <typename A>
struct hash<vector<bool, A>>: function_type<size_t(vector<bool, A>)>, slow_hash_base {
    constexpr static size_t width = sizeof(size_t) * CHAR_BIT;
    size_t operator()(const vector<bool, A> &v) const noexcept
    {
        const auto e = v.end();
        const auto bitw = e.r.bitw;
        const auto b = v.begin();
        const auto len = e.r.ptr - b.r.ptr + bool(bitw);
        if (bitw != 0)
            *e.r.ptr &= std::numeric_limits<size_t>::max() >> (width - bitw);
        return FNVhash<char>::_1a(reinterpret_cast<const char *>(b.r.ptr),
                                  len * sizeof(size_t));
    }
};

template <char...> struct bool_vector_literal_set;

template <char... cs>
struct bool_vector_literal_set<'0', cs...> {
    void operator()(vector<bool> &v) const noexcept
    {
        bool_vector_literal_set<cs...>()(v);
    }
};

template <char... cs>
struct bool_vector_literal_set<'1', cs...> {
    void operator()(vector<bool> &v) const noexcept
    {
        v[sizeof...(cs)] = true;
        bool_vector_literal_set<cs...>()(v);
    }
};

template <>
struct bool_vector_literal_set<'0'> {
    void operator()(vector<bool> &v) const noexcept {}
};

template <>
struct bool_vector_literal_set<'1'> {
    void operator()(vector<bool> &v) const noexcept { v[0] = true; }
};

namespace bool_vector_literal {
/* big end */
template <char... cs> vector<bool> inline operator""_bv()
{
    vector<bool> v(sizeof...(cs));
    bool_vector_literal_set<cs...>()(v);
    return glglT::move(v);
}

} // namespace bool_vector_literal

} // namespace glglT

#endif // GLGL_VECTOR_H

#ifndef GLGL_FUTURE_H
#define GLGL_FUTURE_H

#include <thread>
#include <atomic>
#include <chrono>
#include <stdexcept>
#include <system_error>
#include "sub/glgl_function.h"
#include "sub/glgl_shared_ptr.h"

namespace glglT {

enum class future_errc {
    broken_promise = 1, future_already_retrieved = 2, promise_already_satisfied = 3, no_state = 4
};

inline const std::error_category &future_category() noexcept;

} // namespace glglT

namespace std {

template <> struct is_error_code_enum<glglT::future_errc> : true_type {};

inline std::error_code make_error_code(glglT::future_errc e)
{
    return std::error_code(static_cast<int>(e), glglT::future_category());
}

inline std::error_condition make_error_condition(glglT::future_errc e)
{
    return std::error_condition(static_cast<int>(e), glglT::future_category());
}

} // namespace std

namespace glglT {

inline const std::error_category &future_category() noexcept
{
    static const struct : std::error_category {
        virtual const char *name() const noexcept { return "future"; }
        virtual std::string message(int condition) const
        {
            switch(condition) {
            case int(future_errc::broken_promise):
                return "Shared state has been abandoned";
            case int(future_errc::future_already_retrieved):
                return "The contents were already accessed";
            case int(future_errc::promise_already_satisfied):
                return "Attempt to store a value twice";
            case int(future_errc::no_state):
                return "No associated state";
            default:
                return "Unknown";
            }
        }
    } category;

    return static_cast<const std::error_category &>(category);
}

class future_error : public std::logic_error {
    std::error_code ec;
public:
    explicit future_error(future_errc fec) noexcept:
        std::logic_error(future_category().message(int(fec))), ec(std::make_error_code(fec)) {}
    const std::error_code &code() const noexcept { return ec; }
};

enum class launch : bool { async = true, deferred = false };

template <typename> struct future_data;

template <typename T, typename F> inline
void set_async(const shared_ptr<future_data<T>> &, launch, F);

struct future_data_base {
    std::thread t;
    function<void()> f;
    std::exception_ptr eptr;
    future_data_base *next;
    std::atomic_bool constructed;
    std::atomic_bool excepted;

    future_data_base(): t(), eptr(nullptr), f(nullptr), constructed(false), excepted(false) {}

    struct aux {
        future_data_base *head;
        constexpr aux() noexcept: head(nullptr) {}
        ~aux()
        {
            while (head != nullptr) {
                future_data_base * next = head->next;
                (head->eptr == nullptr ? &(head->constructed) : &(head->excepted))
                    ->store(true, std::memory_order_release);
                head = next;
            }
        }
    };

    static void at_thread_exit(future_data_base *fdb) noexcept
    {
        static thread_local aux head;

        if (head.head && fdb)
            fdb->next = head.head;
        head.head = fdb;
    }
};

template <typename T>
struct future_data: future_data_base {
    alignas(T) unsigned char data[sizeof(T)];

    future_data() = default;
    ~future_data()
    {
        if (constructed.load(std::memory_order_acquire))
            reinterpret_cast<T *>(data)->~T();
    }

    template <typename F, typename... Args>
    static void set_async(const shared_ptr<future_data> &sp, launch l, F &&f, Args&&... args)
    {
        glglT::set_async(sp, l, [f, args...](future_data *p){ ::new (p->data) T (f(args...)); });
    }
};

template <typename T>
struct future_data<T &>: future_data_base {
    T *p;

    template <typename F, typename... Args>
    static void set_async(const shared_ptr<future_data> &sp, launch l, F &&f, Args&&... args)
    {
        glglT::set_async(sp, l, [f, args...](future_data *p){ p->p = f(args...); });
    }
};

template <>
struct future_data<void>: future_data_base {
    template <typename F, typename... Args>
    static void set_async(const shared_ptr<future_data> &sp, launch l, F &&f, Args&&... args)
    {
        glglT::set_async(sp, l, [f, args...](future_data *p){ f(args...); });
    }
};

template <typename T, typename F> inline
void set_async(const shared_ptr<future_data<T>> &sp, launch l, F f)
{
    auto data_p = sp.get();
    auto packed_f = [data_p, f]() mutable -> void {
        std::atomic_bool *b;
        try {
            f(data_p);
            data_p->constructed.store(true, std::memory_order_release);
        } catch(...) {
            data_p->eptr = std::current_exception();
            data_p->excepted.store(true, std::memory_order_release);
        }
    };
    if (l == launch::async) {
        sp->t = std::thread(packed_f);
        sp->t.detach();
    } else
        sp->f = packed_f;
}

enum class future_status { ready, timeout, deferred };

template <typename> struct future;

template <typename> struct promise_base;

template <typename F, typename... Args>
struct async_aux: async_aux<launch, F, Args...> {
    static typename async_aux<launch, F, Args...>::type set_async(F f, Args... args)
    {
        return async_aux<launch, F, Args...>::
            set_async(launch::async, glglT::forward<F>(f), glglT::forward<Args>(args)...);
    }
};

template <typename... Args> inline
typename async_aux<typename decay<Args>::type...>::type async(Args&&...args)
{
    return async_aux<typename decay<Args>::type...>::set_async(glglT::forward<Args>(args)...);
}

template <typename T>
class future_base {
    template <typename F, typename... Args> friend struct async_aux;
    friend struct promise_base<T>;
protected:
    shared_ptr<future_data<T>> dptr;

    constexpr future_base() noexcept: dptr() {}
    future_base(const future_base &o) noexcept: dptr(o.dptr) {}
    future_base(future_base &&o) noexcept: dptr(glglT::move(o.dptr)) {}
    future_base &operator=(const future_base &o) noexcept
    {
        dptr = o.dptr;
        return *this;
    }

    future_base &operator=(future_base &&o) noexcept
    {
        dptr = glglT::move(o.dptr);
        return *this;
    }

    ~future_base()
    {
        if (dptr.use_count() == 1)
            try {
                this->wait();
            } catch(...) {}
    }
public:
    bool valid() const noexcept { return dptr; }
    void wait() const
    {
        if (dptr == nullptr)
            throw future_error(future_errc::no_state);
        if (dptr->f) {
            dptr->f();
            dptr->f = nullptr;
        } else {
            while (dptr->constructed.load(std::memory_order_acquire) == false) {
                if (dptr->excepted.load(std::memory_order_acquire))
                    std::rethrow_exception(dptr->eptr);
                std::this_thread::yield();
            }
        }
    }

    template <typename R, typename P>
    future_status wait_for(const std::chrono::duration<R, P> &d) const
    {
        if (dptr == nullptr)
            throw future_error(future_errc::no_state);
        std::this_thread::sleep_for(d);
        return this->status();
    }

    template <typename C, typename D>
    future_status wait_until(const std::chrono::time_point<C, D> &p) const
    {
        if (dptr == nullptr)
            throw future_error(future_errc::no_state);
        std::this_thread::sleep_until(p);
        return this->status();
    }
private:
    future_status status() const noexcept
    {
        if (dptr->constructed.load(std::memory_order_acquire))
            return future_status::ready;
        if (dptr->f)
            return future_status::deferred;
        return future_status::timeout;
    }
};

template <typename> struct shared_future;

template <typename T>
struct future_common: future_base<T> {
    friend class shared_future<T>;
    shared_future<T> share() noexcept;
protected:
    constexpr future_common() = default;
    future_common(const future_common &) = delete;
    future_common(future_common &&) noexcept = default;
    future_common &operator=(const future_common &) = delete;
    future_common &operator=(future_common &&) noexcept = default;
};

template <typename T>
struct future: future_common<T> {
    T get()
    {
        this->wait();
        auto sp = glglT::move(this->dptr);
        return glglT::move(*reinterpret_cast<T *>(sp->data));
    }
};

template <typename T>
struct future<T &>: future_common<T &> {
    T &get()
    {
        this->wait();
        auto sp = glglT::move(this->dptr);
        return *(sp->p);
    }
};

template <>
struct future<void>: future_common<void> {
    void get()
    {
        this->wait();
        auto sp = glglT::move(this->dptr);
    }
};

template <typename F, typename... Args>
struct async_aux<launch, F, Args...> {
    typedef future<typename result_of<F(Args...)>::type> type;
    static type set_async(launch l, F f, Args... args)
    {
        typedef typename result_of<F(Args...)>::type T;
        auto sp = glglT::make_shared<future_data<T>>();
        if (sp == nullptr)
            throw std::bad_alloc();
        future_data<T>::set_async(sp, l, glglT::forward<F>(f), glglT::forward<Args>(args)...);
        future<T> handle;
        static_cast<future_base<T> &>(handle).dptr = glglT::move(sp);
        return handle;
    }
};

template <typename T>
struct shared_future: future_base<T> {
    constexpr shared_future() noexcept {}
    explicit shared_future(future<T> &&o) noexcept { this->dptr.swap(o.dptr); }
    shared_future &operator=(future<T> &&o) noexcept
    {
        this->dptr = glglT::move(o.dptr);
        return *this;
    }

    const T &get()
    {
        this->wait();
        return *reinterpret_cast<const T *>(this->dptr->data);
    }
};

template <typename T>
struct shared_future<T &>: future_base<T &> {
    constexpr shared_future() noexcept {}
    explicit shared_future(future<T &> &&o) noexcept { this->dptr.swap(o.dptr); }
    shared_future &operator=(future<T &> &&o) noexcept
    {
        this->dptr = glglT::move(o.dptr);
        return *this;
    }

    T &get()
    {
        this->wait();
        return *(this->dptr->p);
    }
};

template <>
struct shared_future<void>: future_base<void> {
    constexpr shared_future() noexcept {}
    explicit shared_future(future<void> &&o) noexcept { this->dptr.swap(o.dptr); }
    shared_future &operator=(future<void> &&o) noexcept
    {
        this->dptr = glglT::move(o.dptr);
        return *this;
    }

    void get() { this->wait(); }
};

template <typename T> inline shared_future<T> future_common<T>::share() noexcept
{
    return shared_future<T>(glglT::move(*this));
}

template <typename T>
class promise_base {
protected:
    shared_ptr<future_data<T>> dptr;
    bool released = false;
    bool already = false;

    promise_base(): dptr(glglT::make_shared<future_data<T>>()) {}
    promise_base(const promise_base &) = delete;
    promise_base(promise_base &&o) noexcept:
        dptr(glglT::move(o.dptr)), released(o.released), already(o.already)
    {
        o.released = o.already = false;
    }

    ~promise_base() { this->free(); }
    promise_base &operator=(const promise_base &) = delete;
    promise_base &operator=(promise_base &&o) noexcept
    {
        this->free();
        dptr = glglT::move(o.dptr);
        released = o.released;
        already = o.already;
        o.released = o.already = false;
        return *this;
    }

    void set_check()
    {
        if (dptr == nullptr)
            throw future_error(future_errc::no_state);
        if (already)
            throw future_error(future_errc::promise_already_satisfied);
        already = true;
    }

    void set_exception(std::exception_ptr eptr)
    {
        this->set_check();
        dptr->eptr = glglT::move(eptr);
        dptr->excepted.store(true, std::memory_order_release);
    }

    void set_exception_at_thread_exit(std::exception_ptr eptr)
    {
        this->set_check();
        dptr->eptr = glglT::move(eptr);
        future_data_base::at_thread_exit(dptr.get());
    }

    void free() noexcept
    {
        if (already == false && dptr) {
            try {
                throw future_error(future_errc::broken_promise);
            } catch(...) {
                dptr->eptr = std::current_exception();
                dptr->excepted.store(true, std::memory_order_release);
            }
        }
    }
public:
    void swap(promise_base &o) noexcept
    {
        glglT::swap(dptr, o.dptr);
        glglT::swap(released, o.released);
        glglT::swap(already, o.already);
    }

    future<T> get_future()
    {
        if (dptr == nullptr)
            throw future_error(future_errc::no_state);
        if (released)
            throw future_error(future_errc::future_already_retrieved);
        released = true;
        future<T> handle;
        static_cast<future_base<T> &>(handle).dptr = dptr;
        return handle;
    }
};

template <typename T>
struct promise: promise_base<T> {
    void set_value(const T &t)
    {
        this->set_check();
        this->already = false;
        ::new (this->dptr->data) T (t);
        this->already = true;
        this->dptr->constructed.store(true, std::memory_order_release);
    }

    void set_value(T &&t)
    {
        this->set_check();
        this->already = false;
        ::new (this->dptr->data) T (glglT::move(t));
        this->already = true;
        this->dptr->constructed.store(true, std::memory_order_release);
    }

    void set_value_at_thread_exit(const T &t)
    {
        this->set_check();
        this->already = false;
        ::new (this->dptr->data) T (t);
        this->already = true;
        future_data_base::at_thread_exit(this->dptr.get());
    }

    void set_value_at_thread_exit(T &&t)
    {
        this->set_check();
        this->already = false;
        ::new (this->dptr->data) T (glglT::move(t));
        this->already = true;
        future_data_base::at_thread_exit(this->dptr.get());
    }

    void set_exception(std::exception_ptr eptr)
    {
        this->promise_base<T>::set_exception(glglT::move(eptr));
    }

    void set_exception_at_thread_exit(std::exception_ptr eptr)
    {
        this->promise_base<T>::set_exception_at_thread_exit(glglT::move(eptr));
    }
};

template <typename T>
struct promise<T &>: promise_base<T &> {
    void set_value(T &t)
    {
        this->set_check();
        this->dptr->p = glglT::addressof(t);
        this->dptr->constructed.store(true, std::memory_order_release);
    }

    void set_value_at_thread_exit(T &t)
    {
        this->set_check();
        this->dptr->p = glglT::addressof(t);
        future_data_base::at_thread_exit(this->dptr.get());
    }

    void set_exception(std::exception_ptr eptr)
    {
        this->promise_base<T &>::set_exception(glglT::move(eptr));
    }

    void set_exception_at_thread_exit(std::exception_ptr eptr)
    {
        this->promise_base<T &>::set_exception_at_thread_exit(glglT::move(eptr));
    }
};

template <>
struct promise<void>: promise_base<void> {
    void set_value()
    {
        this->set_check();
        this->dptr->constructed.store(true, std::memory_order_release);
    }

    void set_value_at_thread_exit()
    {
        this->set_check();
        future_data_base::at_thread_exit(this->dptr.get());
    }

    void set_exception(std::exception_ptr eptr)
    {
        this->promise_base<void>::set_exception(glglT::move(eptr));
    }

    void set_exception_at_thread_exit(std::exception_ptr eptr)
    {
        this->promise_base<void>::set_exception_at_thread_exit(glglT::move(eptr));
    }
};

template <typename T> inline
void swap(promise<T> &p1, promise<T> &p2) noexcept
{
    p1.swap(p2);
}

template <typename T>
struct packaged_task_base: promise_base<T> {
protected:
    template <typename... Args>
    void set_value_trivially(function<T (Args...)> &task, Args... args)
    {
        ::new (this->dptr->data) T (task(glglT::forward<Args>(args)...));
    }
};

template <typename T>
struct packaged_task_base<T &>: promise_base<T &> {
protected:
    template <typename... Args>
    void set_value_trivially(function<T &(Args...)> &task, Args... args)
    {
        this->dptr->p = task(glglT::forward<Args>(args)...);
    }
};

template <>
struct packaged_task_base<void>: promise_base<void> {
protected:
    template <typename... Args>
    void set_value_trivially(function<void (Args...)> &task, Args... args)
    {
        task(glglT::forward<Args>(args)...);
    }
};

template <typename> struct packaged_task;

template <typename R, typename... Args>
class packaged_task<R (Args...)>: public packaged_task_base<R> {
    function<R (Args...)> task;
public:
    packaged_task() = default;
    packaged_task(const packaged_task &) = delete;
    packaged_task(packaged_task &&o) noexcept = default;
    template <typename F>
    explicit packaged_task(F &&f):
        packaged_task_base<R>(), task(glglT::forward<F>(f)) {}

    bool valid() const noexcept { return task; }

    void reset()
    {
        if (task == nullptr)
            throw future_error(future_errc::no_state);
        auto new_sp = glglT::make_shared<future_data<R>>();
        this->free();
        this->release = this->already = false;
        this->sp = glglT::move(new_sp);
    }

    void swap(packaged_task &o) noexcept
    {
        glglT::swap(task, o.task);
        this->promise_base<R>::swap(o);
    }

    void operator()(Args... args)
    {
        if (task == nullptr)
            throw future_error(future_errc::no_state);
        this->set_check();
        try {
            this->set_value_trivially(task, glglT::forward<Args>(args)...);
            this->dptr->constructed.store(true, std::memory_order_release);
        } catch(...) {
            this->set_exception(std::current_exception());
        }
    }

    void make_ready_at_thread_exit(Args... args)
    {
        if (task == nullptr)
            throw future_error(future_errc::no_state);
        this->set_check();
        try {
            this->set_value_trivially(task, glglT::forward<Args>(args)...);
            future_data_base::at_thread_exit(this->dptr.get());
        } catch(...) {
            this->set_exception(std::current_exception());
        }
    }
};

template <typename R, typename... Args> inline
void swap(packaged_task<R (Args...)> &p1, packaged_task<R (Args...)> &p2) noexcept
{
    p1.swap(p2);
}

} // namespace glglT

#endif // GLGL_FUTURE_H
